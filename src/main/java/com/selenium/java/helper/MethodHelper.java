package com.selenium.java.helper;

import com.selenium.java.base.BaseTest;
import com.selenium.java.listeners.TestListener;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.time.Duration;


public class MethodHelper extends BaseTest {

    // region tapCoordinates
    public void tapCoordinates(double point_x, double point_y) {
        WebDriver driver = getDriver();
        new TouchAction((PerformsTouchActions) driver).tap(PointOption.point((int) point_x, (int) point_y)).perform();
    }
    // endregion

    // region ScreenShot
    public void getScreenShot(String text) {
        WebDriver driver = getDriver();
        File file = new File("imagesFromTests/");
        String imagePath = file.getAbsolutePath();
        File srcFile;
        File targetFile;
        String pathToNewFile = imagePath + "/" + text;
        srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        targetFile = new File(pathToNewFile);
        try {
            FileUtils.copyFile(srcFile, targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // endregion

    // region Swipe
    public boolean swipeToElementById(String id) {
        WebDriverWait wait = getWaitDriver(1);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public boolean swipeToElementByText(String text) {
        WebDriverWait wait = getWaitDriver(1);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@text, '" + text + "')]")));
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public void swipeByCordinates(double start_x, double start_y, double end_x, double end_y) {
        WebDriver driver = BaseTest.getDriver();
        new TouchAction((PerformsTouchActions) driver).press(PointOption.point((int) start_x, (int) start_y))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
                .moveTo(PointOption.point((int) end_x, (int) end_y)).release().perform();
    }


    public void swipeInDirection(direction direction, String place, double power) {
        WebDriver driver = BaseTest.getDriver();
        int res[] = getResolutionHandler();
        int screenHeight = (int) Array.get(res, 0);
        int screenWidth = (int) Array.get(res, 1);
        int startY, startX, endX, endY;
        double multiplierX;
        double multiplierY;
        switch (place) {
            case "left":
                multiplierX = 0.2;
                multiplierY = 0.5;
                break;
            case "right":
                multiplierX = 0.8;
                multiplierY = 0.5;
                break;
            case "up":
                multiplierX = 0.5;
                multiplierY = 0.2;
                break;
            case "down":
                multiplierX = 0.5;
                multiplierY = 0.8;
                break;
            default:
                multiplierX = 0.5;
                multiplierY = 0.5;
                break;
        }
        switch (direction) {
            case DOWN:
                startX = (int) (screenWidth * multiplierX);
                startY = (int) (screenHeight * 0.2);
                endX = (int) (screenWidth * multiplierX);
                endY = (int) (screenHeight * power);
                break;
            case UP:
                startX = (int) (screenWidth * multiplierX);
                startY = (int) (screenHeight * power);
                endX = (int) (screenWidth * multiplierX);
                endY = (int) (screenHeight * 0.2);
                break;
            case RIGHT:
                startX = (int) (screenWidth * power);
                startY = (int) (screenHeight * multiplierY);
                endX = (int) (screenWidth * 0.2);
                endY = (int) (screenHeight * multiplierY);
                break;
            case LEFT:
                startX = (int) (screenWidth * 0.2);
                startY = (int) (screenHeight * multiplierY);
                endX = (int) (screenWidth * power);
                endY = (int) (screenHeight * multiplierY);
                break;
            default:
                throw new IllegalArgumentException("Incorrect direction: " + direction);
        }
        new TouchAction((PerformsTouchActions) driver).press(PointOption.point(startX, startY))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(endX, endY)).release().perform();
    }


// endregion


    public void swipeToElementByTextAndCordinates(String text, int start_x, int start_y, int end_x,
                                                  int end_y){
        WebDriver driver = BaseTest.getDriver();

        while (swipeToElementByText(text)) {
            new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point(start_x, start_y))
                    .moveTo(PointOption.point(end_x, end_y)).release().perform();  }
    }
// endregion

    //region Scroll Methods

    public void scrollId(String element, BaseTest.direction direct, String start, double power, int iteration) {
        int i = 0;
        while (swipeToElementById(element) && i < iteration) {
            swipeInDirection(direct, start, power);
            i++;
        }
    }

    public void scrollText(String element, BaseTest.direction direct, String start, double power, int iteration) {
        int i = 0;
        while (swipeToElementByText(element) && i < iteration) {
            swipeInDirection(direct, start, power);

        }
    }

//endregion

    //region longPress
    public void longPress(double point_x, double point_y) {
        WebDriver driver = getDriver();
        new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point((int) point_x, (int) point_y)).perform();
    }

    public void dragAndDrop(double start_x, double start_y, double end_x, double end_y) {
        WebDriver driver = getDriver();
        TouchAction action = new TouchAction((PerformsTouchActions) driver);
        action.longPress(PointOption.point((int) start_x, (int) start_y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
                .moveTo(PointOption.point((int) end_x, (int) end_y)).release().perform();
    }
    //endregion longPress

    //region backBtn
    public void pressAndroidBackBtn() {
        WebDriver driver = BaseTest.getDriver();
        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
    }
    //endregion backBtn

    //region waitTime
    public void waitTime(int x) {
        try {
            Thread.sleep(x * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    //endregion waitTime


    //region uploadImage

    public void uploadImage(String path) throws AWTException {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        StringSelection ss = new StringSelection(path);

        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

        Robot robot = new Robot();
        robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
        robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
        robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
        robot.keyPress(java.awt.event.KeyEvent.VK_V);
        robot.keyRelease(java.awt.event.KeyEvent.VK_V);
        robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
        robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
        robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
    }
    //endregion uploadImage

    //region testScreenshot
    public void testScreenshot(String name) {
        WebDriver driver = getDriver();
        TestListener testListener = new TestListener();

        if (driver instanceof WebDriver) {
            System.out.println("Screenshot captured for test:" + name);
            testListener.saveScreenshotPNG(driver);
        }
        testListener.saveTextLog("Screenshot after test: " + name);
    }
    //endregion testScreenshot


    public void testScreenShoot(String classname) {
        WebDriver driver = getDriver();
        com.selenium.java.listeners.TestListener testListener = new TestListener();
        if (driver instanceof  WebDriver) {
            testListener.saveScreenshotPNG(driver);
        }
        testListener.saveTextLog("Test: "+ classname + " failed and screenshot taken!");
    }
}

