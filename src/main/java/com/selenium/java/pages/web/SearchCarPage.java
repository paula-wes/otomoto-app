package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class SearchCarPage extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_enum_make\"]")
    public WebElement input_brand;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-0-item-0\"]/div[1]/span[1]")
    public WebElement btn_brand;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_enum_model\"]")
    public WebElement input_model;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-1-item-0\"]/div[1]/span[1]")
    public WebElement btn_model;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_enum_generation\"]")
    public WebElement input_generation;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-2-item-0\"]/div[1]/span[1]")
    public WebElement btn_generation;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_price:from\"]")
    public WebElement input_price_from;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-3-item-0\"]/div[1]/span[1]")
    public WebElement btn_price_from;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_price:to\"]")
    public WebElement input_price_to;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-4-item-0\"]/div[1]/span[1]")
    public WebElement btn_price_to;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_year:from\"]")
    public WebElement input_year_from;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-5-item-0\"]/div[1]/span[1]")
    public WebElement btn_year_from;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_year:to\"]")
    public WebElement input_year_to;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-6-item-0\"]/div[1]/span[1]")
    public WebElement btn_year_to;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_mileage:from\"]")
    public WebElement input_mileage_from;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-7-item-0\"]/div[1]/span[1]")
    public WebElement btn_mileage_from;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_mileage:to\"]")
    public WebElement input_mileage_to;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-8-item-0\"]/div[1]/span[1]")
    public WebElement btn_mileage_to;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_enum_fuel_type\"]")
    public WebElement input_fuel_type;

    @FindBy(how = How.XPATH, using = "//*[@id=\"downshift-9-item-0\"]/div[1]/span[1]")
    public WebElement btn_fuel_type;

    @FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/main/div[3]/article/div/form/div[1]/div[8]" +
            "/div/label/div")
    public WebElement checkbox_vin;

    @FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/main/div[3]/article/div/form/div[2]/button[1]")
    public WebElement btn_show;

    @Step("Setting brand of car")
    public void searchWebBrand(String brand){

       input_brand.sendKeys(brand);
       btn_brand.click();
    }

    @Step("Setting model of car")
    public void searchWebModel(String model){

        input_model.sendKeys(model);
        btn_model.click();

    }

    @Step("Setting generation")
    public void searchWebGeneration(String generation){

        input_generation.sendKeys(generation);
        btn_generation.click();
    }

    @Step("Setting min and max price")
    public void searchWebPrice(String price_from, String price_to){

        input_price_from.sendKeys(price_from);
        btn_price_from.click();
        methodHelper.waitTime(2);
        input_price_to.sendKeys(price_to);
        btn_price_to.click();

    }

    @Step("Setting production year")
    public void searchWebYear(String year_from, String year_to){

        input_year_from.sendKeys(year_from);
        btn_year_from.click();
        methodHelper.waitTime(2);
        input_year_to.sendKeys(year_to);
        btn_year_to.click();
    }

    @Step("Setting car mileage")
    public void searchWebMileage(String mileage_from, String mileage_to){

        input_mileage_from.sendKeys(mileage_from);
        btn_mileage_from.click();
        methodHelper.waitTime(2);
        input_mileage_to.sendKeys(mileage_to);
        btn_mileage_to.click();


    }

    @Step("Setting type of fuel")
    public void searchWebTypeOfFuel(String fuel){

        input_fuel_type.sendKeys(fuel);
        btn_fuel_type.click();

    }

    @Step("Setting car VIN")
    public void searchWebVin(boolean vin){

        if(vin){
            checkbox_vin.click();
        }

    }

    @Step("Show ads")
    public void searchWebShow(){

    btn_show.click();

    }

    @Step("Checking assertion")
    public void searchWebAssert(){
        //*[@id="saveSearchCriteria"]/span[2]
        try {
            getDriver().findElement(By.id("param_country_origin"));
        }catch (java.util.NoSuchElementException e) {
            Assert.fail("Error on Page:element not found. Failed searching");
        }
        Assert.assertTrue(getDriver().findElement(By.id("param_country_origin")).isDisplayed());
    }

}
