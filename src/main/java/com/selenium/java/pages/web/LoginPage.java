package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class LoginPage extends BaseTest {

    @FindBy(how = How.ID, using = "userEmail")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "userPass")
    public WebElement input_password;

    @FindBy(how = How.XPATH, using = "//*[@id=\"loginForm\"]/fieldset/div[5]/label")
    public WebElement checkbox_remember;

    @FindBy(how = How.ID, using = "se_userLogin")
    public WebElement btn_sing_in;

    @FindBy(how = How.XPATH, using = "//*[@id=\"se_accountAnswers\"]")
    public WebElement btn_message;

    @Step("Setting e-mail")
    public void loginSetEmail(String email){
        input_email.sendKeys(email);
    }

    @Step("Setting password")
    public void loginSetPassword(String pass){
        input_password.sendKeys(pass);
    }

    @Step("Checking remember me checkbox")
    public void loginRememberMe(boolean remember){
        if(remember){
            checkbox_remember.click();
        }
    }

    @Step("Sing In")
    public void loginSingIn(){
        btn_sing_in.click();
    }

    @Step("Checking assertion for correct login")
    public void loginAssert(){
        WebDriver driver = getDriver();
        try {
            driver.findElement(By.id("se_accountAnswers"));
        }catch (java.util.NoSuchElementException e) {
            Assert.fail("Error on Page:element not found");
        }
        Assert.assertTrue(  driver.findElement(By.id("se_accountAnswers")).isDisplayed());
    }

    @Step("Checking assertion for fail login")
    public void loginFailAssert(){
        WebDriver driver = getDriver();
        try {
            driver.findElement(By.id("se_userLogin"));
        }catch (java.util.NoSuchElementException e) {
            Assert.fail("Error on Page:element not found");
        }
        Assert.assertTrue(  driver.findElement(By.id("se_userLogin")).isDisplayed());
    }
}
