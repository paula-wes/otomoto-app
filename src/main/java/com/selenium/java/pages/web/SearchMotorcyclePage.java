package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class SearchMotorcyclePage extends BaseTest {


    MethodHelper methodHelper = new MethodHelper();

    @FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/main/div[3]/article/ul/li[3]/a/span")
    public WebElement btn_motorcycle;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_enum_make\"]")
    public WebElement input_brand;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/main/div[3]/article/div/form/div[1]/div[1]/div/div/" +
            "div/div/div/div[1]/div[1]/div/span")
    public WebElement btn_brand;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_enum_model\"]")
    public WebElement input_model;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/main/div[3]/article/div/form/div[1]/div[2]" +
            "/div/div/div/div/div/div/div/span")
    public WebElement btn_model;


    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_price:from\"]")
    public WebElement input_price_from;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/main/div[3]/article/div/form/div[1]/div[3]/fieldset" +
            "/div/div[1]/div/div/div/div[1]/div/span")
    public WebElement btn_price_from;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_price:to\"]")
    public WebElement input_price_to;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/main/div[3]/article/div/form/div[1]/div[3]/fieldset" +
            "/div/div[2]/div/div/div/div/div/span")
    public WebElement btn_price_to;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_year:from\"]")
    public WebElement input_year_from;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/main/div[3]/article/div/form/div[1]/div[4]/fieldset" +
            "/div/div[1]/div/div/div/div[1]/div/span")
    public WebElement btn_year_from;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_year:to\"]")
    public WebElement input_year_to;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/main/div[3]/article/div/form/div[1]/div[4]/fieldset" +
            "/div/div[2]/div/div/div/div/div/span")
    public WebElement btn_year_to;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_engine_capacity:from\"]")
    public WebElement input_displacement_from;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/main/div[3]/article/div/form/div[1]/div[5]/fieldset" +
            "/div/div[1]/div/div/div/div/div/span")
    public WebElement btn_displacement_from;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_float_engine_capacity:to\"]")
    public WebElement input_displacement_to;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/main/div[3]/article/div/form/div[1]/div[5]/fieldset/" +
            "div/div[2]/div/div/div/div[1]/div/span")
    public WebElement btn_displacement_to;

    @FindBy(how = How.XPATH, using = "//*[@id=\"filter_enum_fuel_type\"]")
    public WebElement input_fuel_type;

    @FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/main/div[3]/article/div/form/div[1]/" +
            "div[6]/div/div/div/div/div/div/div/span")
    public WebElement btn_fuel_type;

    @FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/main/div[3]/article/div/form/div[2]/button[1]")
    public WebElement btn_show;

    @FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/div[1]/div/div[2]/button")
    public WebElement btn_cookies;

    @Step("Choosing motorcycle")
    public void searchWebMotorcycle(){
        btn_motorcycle.click();
    }
    @Step("Accepting cookies")
    public void searchCookies(){
        btn_cookies.click();
    }


    @Step("Setting brand of car")
    public void searchWebBrand(String brand){

        input_brand.sendKeys(brand);
        methodHelper.waitTime(2);
        btn_brand.click();
    }

    @Step("Setting model of car")
    public void searchWebModel(String model){

        input_model.sendKeys(model);
        btn_model.click();

    }


    @Step("Setting min and max price")
    public void searchWebPrice(String price_from, String price_to){

        input_price_from.sendKeys(price_from);
        btn_price_from.click();
        methodHelper.waitTime(2);
        input_price_to.sendKeys(price_to);
        btn_price_to.click();

    }

    @Step("Setting production year")
    public void searchWebYear(String year_from, String year_to){

        input_year_from.sendKeys(year_from);
        btn_year_from.click();
        methodHelper.waitTime(2);
        input_year_to.sendKeys(year_to);
        btn_year_to.click();
    }

    @Step("Setting car engine capacity(")
    public void searchWebEngineCapacity(String dis_from, String dis_to){

        input_displacement_from.sendKeys(dis_from);
        btn_displacement_from.click();
        methodHelper.waitTime(2);
        input_displacement_to.sendKeys(dis_to);
        btn_displacement_to.click();


    }

    @Step("Setting type of fuel")
    public void searchWebTypeOfFuel(String fuel){

        input_fuel_type.sendKeys(fuel);
        btn_fuel_type.click();

    }


    @Step("Show ads")
    public void searchWebShow(){

        btn_show.click();

    }

    @Step("Checking assertion")
    public void searchWebAssert(){
        //*[@id="saveSearchCriteria"]/span[2]
        try {
            getDriver().findElement(By.id("param_country_origin"));
        }catch (java.util.NoSuchElementException e) {
            Assert.fail("Error on Page:element not found. Failed searching");
        }
        Assert.assertTrue(getDriver().findElement(By.id("param_country_origin")).isDisplayed());
    }

}
