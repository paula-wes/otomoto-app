package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

public class SearchCarsScreen extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    //region BUTTONS
    @FindBy(how = How.ID, using = "pl.otomoto:id/banner_covid_description")
    public WebElement btn_banner;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Odwiedź OTOMOTO')]")
    public WebElement btn_back_to_otomoto;

    @FindBy(how = How.ID, using = "pl.otomoto:id/tv_select_title")
    public WebElement btn_type_of_car;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_filter;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement checkbox_default;

    @FindBy(how = How.ID, using = "pl.otomoto:id/text")
    public WebElement checkbox_colour;

    @FindBy(how = How.ID, using = "pl.otomoto:id/md_buttonDefaultPositive")
    public WebElement btn_ok;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Marka pojazdu')]")
    public WebElement btn_brand_of_car;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Model pojazdu')]")
    public WebElement btn_model_of_car;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_min")
    public List<WebElement> input_min;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_max")
    public List<WebElement> input_max;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Rodzaj paliwa')]")
    public WebElement btn_type_of_fuel;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Prywatnie lub firma')]")
    public WebElement btn_privacy_of_car;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Waluta')]")
    public WebElement btn_currency_of_car;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Kraj pochodzenia')]")
    public WebElement btn_country_of_origin;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Skrzynia biegów')]")
    public WebElement btn_transmission;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Napęd')]")
    public WebElement btn_drive;

    @FindBy(how = How.ID, using = "pl.otomoto:id/show_more_filters")
    public WebElement btn_show_more;

    @FindBy(how = How.ID, using = "pl.otomoto:id/switch_value")
    public List<WebElement> list_switch;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodatkowe wyposażenie')]")
    public WebElement btn_additional_equipment;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Kolor')]")
    public WebElement btn_colours;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Kierownica po prawej')]")
    public WebElement btn_wheel;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Tak')]")
    public WebElement btn_wheel_yes;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Nie')]")
    public WebElement btn_wheel_no;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Filtr cząstek stałych')]")
    public WebElement btn_filter;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Metalik')]")
    public WebElement btn_metalic;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Perłowy')]")
    public WebElement btn_pearl;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Matowy')]")
    public WebElement btn_mat;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Akryl')]")
    public WebElement btn_acrylic;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Autoryzowanego Dealera')]")
    public WebElement btn_dealer;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'VAT marża')]")
    public WebElement btn_margin;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Faktura VAT')]")
    public WebElement btn_invoice;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Możliwość finansowania')]")
    public WebElement btn_funding;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Leasing')]")
    public WebElement btn_leasing;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Pokaż oferty z numerem VIN')]")
    public WebElement btn_vin;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Ma numer rejestracyjny')]")
    public WebElement btn_reg_number;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Uszkodzony')]")
    public WebElement btn_damaged;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Zarejestrowany w Polsce')]")
    public WebElement btn_reg_in_pol;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Pierwszy właściciel')]")
    public WebElement btn_first_owner;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Bezwypadkowy')]")
    public WebElement btn_no_collision;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Serwisowany w ASO')]")
    public WebElement btn_aso;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Zarejestrowany jako zabytek')]")
    public WebElement btn_relic;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Tuning')]")
    public WebElement btn_tuning;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Homologacja ciężarowa')]")
    public WebElement btn_approval;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wybierz lokalizację')]")
    public WebElement btn_localization;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Pokaż wyniki')]")
    public WebElement btn_show_results;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Obserwuj to wyszukiwanie')]")
    public WebElement btn_assert;

    //endregion BUTTONS

    //region METHODS
    @Step("Checking covid banner")
    public void carsBannerClick() {
        btn_banner.click();
        btn_back_to_otomoto.click();
    }

    @Step("Setting type of car")
    public void carsSetTypeOfCar(String type) {
        btn_type_of_car.click();
        input_filter.sendKeys(type);
        checkbox_default.click();
        btn_ok.click();
    }

    @Step("Setting brand of car")
    public void carsSetBrandOfCar(String brand) {
        btn_brand_of_car.click();
        input_filter.sendKeys(brand);
        checkbox_default.click();
        btn_ok.click();
    }

    @Step("Setting model of car")
    public void carsSetModelOfCar(String model) {
        btn_model_of_car.click();
        input_filter.sendKeys(model);
        checkbox_default.click();
        btn_ok.click();
    }

    @Step("Setting min and max price")
    public void carsSetPrice(String min, String max) {
        for (int d = 0; d < input_min.size(); d++) {
            input_min.get(0).sendKeys(min);
            input_max.get(0).sendKeys(max);
        }
    }

    @Step("Setting production year")
    public void carsSetProductionYear(String from, String to) {
        for (int d = 0; d < input_min.size(); d++) {
            input_min.get(0).sendKeys(from);
            input_max.get(0).sendKeys(to);
        }
    }

    @Step("Setting type of fuel")
    public void carsSetTypeOfFuel(String fuel) {
        btn_type_of_fuel.click();
        input_filter.sendKeys(fuel);
        checkbox_default.click();
        btn_ok.click();
    }

    @Step("Setting privacy")
    public void carsPrivacy(String privacy) {

        btn_privacy_of_car.click();
        input_filter.sendKeys(privacy);
        checkbox_default.click();
        btn_ok.click();

    }

    @Step("Setting currency")
    public void carsCurrency(String currency) {

        btn_currency_of_car.click();
        input_filter.sendKeys(currency);
        checkbox_default.click();
        btn_ok.click();

    }

    @Step("Setting car mileage")
    public void carsMileage(String from, String to) {
        for (int d = 0; d < input_max.size(); d++) {
            if (d == 1) {
                input_min.get(0).sendKeys(from);
                input_max.get(0).sendKeys(to);
            }
        }
    }

    @Step("Setting country of origin")
    public void carsCountryOfOrigin(String country) {
        btn_country_of_origin.click();
        input_filter.sendKeys(country);
        checkbox_default.click();
        btn_ok.click();
    }

    @Step("Click on show more button")
    public void carsShowMore() {

        btn_show_more.click();

    }

    @Step("Setting displacement")
    public void carsDisplacement(String from, String to) {
        for (int d = 0; d < input_min.size(); d++) {
            if (d == 0) {
                input_min.get(1).sendKeys(from);
                input_max.get(1).sendKeys(to);
            }
        }
    }

    @Step("Setting power of engine")
    public void carsEnginePower(String from, String to) {
        for (int d = 0; d < input_min.size(); d++) {
            if (d == 0) {
                input_min.get(2).sendKeys(from);
                input_max.get(2).sendKeys(to);
            }
        }
    }

    @Step("Setting transmission")
    public void carsTransmission(String type) {
        btn_transmission.click();
        input_filter.sendKeys(type);
        checkbox_default.click();
        btn_ok.click();
    }

    @Step("Setting type of drive")
    public void carsDrive(String type) {
        btn_drive.click();
        input_filter.sendKeys(type);
        checkbox_default.click();
        btn_ok.click();
    }

    @Step("Setting particle filter")
    public void carsFilter(boolean filter) {

        if (filter) {
            btn_filter.click();
        }

    }

    @Step("Setting additional equipment")
    public void carsAdditionalEquipment(String first, String second, String third) {
        btn_additional_equipment.click();
        input_filter.sendKeys(first);
        checkbox_default.click();
        input_filter.sendKeys(second);
        checkbox_default.click();
        input_filter.sendKeys(third);
        checkbox_default.click();
        btn_ok.click();
    }

    @Step("Setting colour")
    public void carsColour(String colour) {
        btn_colours.click();
        input_filter.sendKeys(colour);
        checkbox_colour.click();
        btn_ok.click();
    }

    @Step("Setting number of seats")
    public void carsNumberOfSeats(String seats_min, String seats_max) {

        for (int d = 0; d < input_min.size(); d++) {
            if (d == 0) {
                input_min.get(0).sendKeys(seats_min);
                input_max.get(0).sendKeys(seats_max);
            }
        }
    }

    @Step("Setting number of doors")
    public void carsNumberOfDoors(String doors_min, String doors_max) {

        for (int d = 0; d < input_min.size(); d++) {
            if (d == 0) {
                input_min.get(1).sendKeys(doors_min);
                input_max.get(1).sendKeys(doors_max);
            }
        }
    }

    @Step("Setting car body")
    public void carsCarBody(boolean metallic, boolean pearl, boolean mat, boolean acrylic) {


        if (metallic) {

            btn_metalic.click();
        }

        if (pearl) {

            btn_pearl.click();
        }

        if (mat) {
            btn_mat.click();

        }

        if (acrylic) {

            btn_acrylic.click();

        }
    }

    @Step("Setting left or right side wheel")
    public void carsEnglishWheel(boolean wheel) {

        btn_wheel.click();

        if (wheel) {
            btn_wheel_yes.click();
        } else {
            btn_wheel_no.click();
        }
        btn_ok.click();
    }

    @Step("Setting financial information")
    public void carsFinancialInformation(boolean dealer, boolean margin, boolean invoice, boolean funding,
                                         boolean leasing) {


        if (dealer) {

            btn_dealer.click();
        }

        if (margin) {

            btn_margin.click();
        }

        if (invoice) {

            btn_invoice.click();
        }


        if (funding) {

            btn_funding.click();
            methodHelper.scrollText("Leasing", direction.UP, "up", 0.4, 1);
        }
        if (leasing) {

            btn_leasing.click();
        }

    }


    @Step("Setting vehicle condition")
    public void carsVehicleCondition(boolean vin, boolean registration, boolean damaged, boolean reg_in_pl,
                                     boolean first_owner, boolean collision_free, boolean aso, boolean relic,
                                     boolean tuning, boolean approval) {


        if (vin) {
            btn_vin.click();

        }


        if (registration) {

            btn_reg_number.click();
        }

        if (damaged) {

            btn_damaged.click();
        }
        if (reg_in_pl) {

            btn_reg_in_pol.click();
        }

        if (first_owner) {

            btn_first_owner.click();
        }

        if (collision_free) {

            btn_no_collision.click();
            methodHelper.scrollText("Tuning", direction.UP, "up", 0.4, 1);
        } else{
            methodHelper.scrollText("Tuning", direction.UP, "up", 0.4, 1);
        }


        if (aso) {

            btn_aso.click();
        }

        if (relic) {

            btn_relic.click();
        }

        if (tuning) {

            btn_tuning.click();
        }

        if (approval) {

            btn_approval.click();

        }

    }

    @Step("Setting location")
    public void carsSearchLocation(String voivodeship, String city, String distance) {

        btn_localization.click();
        System.out.println("Wybrano zakładkę \"Wybierz lokalizację\" ");

        methodHelper.scrollText(voivodeship, direction.UP, "up", 0.3, 1);

        WebElement chooseVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" +
                voivodeship + "')]"));
        chooseVoivodeship.click();
        System.out.println("Wybrano województwo: " + voivodeship);
        methodHelper.waitTime(2);

        methodHelper.scrollText(city, direction.UP, "up", 0.3, 1);
        WebElement chooseCity = getDriver().findElement(By.xpath("//*[contains(@text, '" + city + "')]"));
        chooseCity.click();

        checkbox_default.click();
        WebElement chooseDistance = getDriver().findElement(By.xpath("//*[contains(@text, '" + distance + "')]"));
        chooseDistance.click();
    }

    @Step("Setting results")
    public void carsSearchShowResults() {
        btn_show_results.click();
    }

    @Step("Checking assertion")
    public void carsAssert(){
        Assert.assertTrue(btn_assert.isDisplayed(), "Searching failed");
    }

    //endregion METHODS
}

