package com.selenium.java.pages.android;


import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

@Feature("Searching constructions vehicles and machines Mobile Tests")
public class SearchTrailersScreen extends BaseTest {

    //region BUTTONS
    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_application;

    @FindBy(how = How.ID, using = "pl.otomoto:id/show_more_filters")
    public WebElement btn_moreParameters;

    @FindBy(how = How.ID, using = "pl.otomoto:id/filter")
    public WebElement input_vehicleBrand;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseBrand;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_text_value")
    public WebElement input_vehicleModel;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_countryOfOrigin;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseCountry;

    @FindBy(how = How.ID, using = "pl.otomoto:id/chooserBtn")
    public WebElement btn_chooseLocation;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_vehicleType;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseType;

    @FindBy(how = How.CLASS_NAME, using = "android.widget.EditText")
    public List<WebElement> list_data;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_min")
    public WebElement input_priceMin;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_max")
    public WebElement input_priceMax;

    //endregion BUTTONS



    //region METHODS


    MethodHelper helper = new MethodHelper();
    WebDriver driver = getDriver();

    @Step("Setting trailers category")
    public void searchSetTrailers() {

        helper.waitTime(3);

        while (helper.swipeToElementByText("PRZYCZEPY")) {
            new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point(984, 312))
                    .moveTo(PointOption.point(108, 316)).release().perform();  }

        helper.waitTime(5);
        WebElement agricultural_cat = getDriver().findElement(By.xpath("//*[contains(@text, '" + "PRZYCZEPY" + "')]"));
        agricultural_cat.click();
        System.out.println("Wybrano zakładkę \"Przyczepy\"");
    }

    @Step("Setting application")
    public void searchApplication(String application) {
        btn_application.click();
        System.out.println("Wybrano zakładkę \"Zastosowanie\"");
        while (helper.swipeToElementByText(application)) {
            new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point(172, 2136))
                    .moveTo(PointOption.point(204, 1000)).release().perform(); }
        WebElement applications = getDriver().findElement(By.xpath("//*[contains(@text, '" + application + "')]"));
        applications.click();
        System.out.println("Wybrano: " + application);
    }

    @Step("Setting model of vehicle")
    public void searchVehicleModel( String vehicleModel) {
        while (helper.swipeToElementByText("Model pojazdu")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }
        WebElement setVehicleModel = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Model pojazdu" + "')]"));
        setVehicleModel.click();
        input_vehicleModel.sendKeys(vehicleModel);
        helper.pressAndroidBackBtn();
        System.out.println("Wpisano model pojazdu: " + vehicleModel);
        helper.waitTime(3);

    }

    @Step("Setting brand of vehicle")
    public void searchVehicleBrand(String vehicleBrand) {
        while (helper.swipeToElementByText("Marka pojazdu")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }
        WebElement chooseVehicleBrand = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Marka pojazdu" + "')]"));
        chooseVehicleBrand.click();
        System.out.println("Wybrano zakładkę \"Marka pojazdu\"");
        input_vehicleBrand.sendKeys(vehicleBrand);
        btn_chooseBrand.click();
        System.out.println("Wybrano: " + vehicleBrand);
        helper.waitTime(3);

    }
    @Step("Setting min and max price")
    public void searchSetPrice(String priceMin, String priceMax) {
        while (helper.swipeToElementByText("Cena")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }
        input_priceMin.sendKeys(priceMin);
        System.out.println("Wybrano cenę minimalną: " + priceMin);
        input_priceMax.sendKeys(priceMax);


        System.out.println("Wybrano cenę maksymalną: " + priceMax);
    }
    @Step("Setting privacy")
    public void searchSetPrivacy(String privacy) {
        WebElement privateOrCorpo = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Prywatnie lub firma" + "')]"));
        privateOrCorpo.click();
        System.out.println("Wybrano zakładkę \"Prywatne lub firma\"");
        WebElement setPrivacy = getDriver().findElement(By.xpath("//*[contains(@text, '" + privacy + "')]"));
        setPrivacy.click();
        System.out.println("Wybrano: " + privacy);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
    }

    @Step("Setting currency")
    public void searchSetCurrency(String currency) {
        while (helper.swipeToElementByText("Waluta")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        WebElement currencies = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Waluta" + "')]"));
        currencies.click();
        System.out.println("Wybrano zakładkę \"Waluta\"");
        WebElement setCurrency = getDriver().findElement(By.xpath("//*[contains(@text, '" + currency + "')]"));
        setCurrency.click();
        System.out.println("Wybrano: " + currency);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }
    @Step("Setting min and max year of production")
    public void searchSetYearOfProduction(int third, int fourth, String yearOfProdFrom, String yearOfProdTo) {
        while (helper.swipeToElementByText("Kraj pochodzenia")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == third) {
                list_data.get(third).sendKeys(yearOfProdFrom);
                list_data.get(fourth).sendKeys(yearOfProdTo);
            }
        }
        System.out.println("Wybrano rok produkcji od: " + yearOfProdFrom);
        System.out.println("Wybrano rok produkcji do: " + yearOfProdTo);
        helper.waitTime(3);
    }

    @Step("Setting country of origin")
    public void searchSetCountryOfOrigin(String country) {
        while (helper.swipeToElementByText("Kraj pochodzenia")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement chooseCountry = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Kraj pochodzenia" + "')]"));
        chooseCountry.click();
        System.out.println("Wybrano zakładkę \"Kraj pochodzenia\"");
        input_countryOfOrigin.sendKeys(country);
        btn_chooseCountry.click();
        System.out.println("Wybrano kraj: " + country);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }
    @Step("Setting min and max allowed package")
    public void searchSetAllowedPackage(int third, int fourth, String packageMin, String packageMax) {
        while (helper.swipeToElementByText("Dopuszczalna ładowność")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == third) {
                list_data.get(third).sendKeys(packageMin);
                list_data.get(fourth).sendKeys(packageMax);
            }
        }
        System.out.println("Wybrano dopuszczalną ładowność od: " + packageMin);
        System.out.println("Wybrano dopuszczalną ładowność do: " + packageMax);
        helper.waitTime(3);
    }

    @Step("Setting min and max capacity / volume")
    public void searchSetCapacity(int seventh, int eighth, String capacityMin, String capacityMax) {
        while (helper.swipeToElementByText("Pojemność (objętość)")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == seventh) {
                list_data.get(seventh).sendKeys(capacityMin);
                list_data.get(eighth).sendKeys(capacityMax);
            }
        }
        System.out.println("Wybrano pojemność/objętość od: " + capacityMin);
        System.out.println("Wybrano pojemność/objętość do: " + capacityMax);
        helper.waitTime(3);
    }


    @Step("Setting number of axles")
    public void searchSetNumberOfAxles( int ninth, int tenth, String axlesMin, String axlesMax) {
        while (helper.swipeToElementByText("Liczba osi")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == ninth) {
                list_data.get(ninth).sendKeys(axlesMin);
                list_data.get(tenth).sendKeys(axlesMax);
            }
        }
        System.out.println("Wybrano liczbę osi od: " + axlesMin);
        System.out.println("Wybrano liczbę osi do: " + axlesMax);
        helper.waitTime(3);

    }
    @Step("Setting double rear wheels")
    public void searchSetDoubleRearWheels(boolean doubleWheels){
        while (helper.swipeToElementByText("Podwójne tylne koła")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        if (doubleWheels){
            WebElement setDoubleWheels = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Podwójne tylne koła" + "')]"));
            setDoubleWheels.click();
            System.out.println("Wybrano opcję: podwójne tylne koła");
     }else{}

    }
    @Step("Setting more parameters")
    public void searchMoreParameters() {
        while (helper.swipeToElementByText("WIĘCEJ PARAMETRÓW")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        btn_moreParameters.click();
        System.out.println("Wybrano zakładkę \"Więcej parametrów\" ");
    }

    @Step("Setting number of axles")
    public void searchSetPermissibleGrossWeight( int eleventh, int twelfth, String grossWeightMin, String grossWeightMax) {
        while (helper.swipeToElementByText("Dopuszczalna masa całkowita")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == eleventh) {
                list_data.get(eleventh).sendKeys(grossWeightMin);
                list_data.get(twelfth).sendKeys(grossWeightMax);
            }
        }
        System.out.println("Wybrano dopuszczalną masę całkowitą od: " + grossWeightMin);
        System.out.println("Wybrano dopuszczalną masę całkowitą do: " + grossWeightMax);
        helper.waitTime(3);

    }

    @Step("Setting financial information")
    public void searchFinancialInformation(boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                           boolean financingOption, boolean leasing, boolean rent) {

        System.out.println("Wybrano zakładkę \"Informacje finansowe\"");
        while (helper.swipeToElementByText("Możliwość finansowania")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }

        if (authorizedDealer) {
            WebElement setAuthorizedDealer = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Autoryzowanego Dealera" + "')]"));
            setAuthorizedDealer.click();
            System.out.println("Wybrano leasing");
        } else {
        }
        if (vatMargin) {
            WebElement setVatMargin = getDriver().findElement(By.xpath("//*[contains(@text, '" + "VAT marża" + "')]"));
            setVatMargin.click();
            System.out.println("Wybrano opcję: VAT marża");
        } else {
        }
        if (invoiceVat) {
            WebElement setInvoiceVat = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Faktura VAT" + "')]"));
            setInvoiceVat.click();
            System.out.println("Wybrano opcję: faktura VAT");
        } else {
        }
        if (financingOption) {
            WebElement setFinancingOption = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Możliwość finansowania" + "')]"));
            setFinancingOption.click();
            System.out.println("Wybrano możliwość finansowania");
        } else {
        }

        if (leasing) {
            WebElement setLeasing = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Leasing" + "')]"));
            setLeasing.click();
            System.out.println("Wybrano leasing");
        } else {
        }
        if (rent) {
            WebElement setRent = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Opcja wynajem" + "')]"));
            setRent.click();
            System.out.println("Wybrano możliwość wynajmu");
        } else {
        }
    }

    @Step("Setting car status")
    public void searchCarStatus(boolean damaged, boolean registerInPoland, boolean firstOwner,
                                boolean noAccidents) {

        System.out.println("Wybrano zakładkę \"Status pojazdu\"");
        while (helper.swipeToElementByText("Bezwypadkowy")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }

        if (damaged) {
            WebElement setDamaged = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Uszkodzony" + "')]"));
            setDamaged.click();
            System.out.println("Wybrano status: Uszkodzony");
        } else {
        }
        if (registerInPoland) {
            WebElement setRegisterInPoland = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Zarejestrowany w Polsce" + "')]"));
            setRegisterInPoland.click();
            System.out.println("Wybrano status: Zarejestrowany w Polsce");
        } else {
        }
        if (firstOwner) {
            WebElement setFirstOwner = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pierwszy właściciel" + "')]"));
            setFirstOwner.click();
            System.out.println("Wybrano status: Pierwszy właściciel");
        } else {
        }
        if (noAccidents) {
            WebElement setNoAccidents = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Bezwypadkowy" + "')]"));
            setNoAccidents.click();
            System.out.println("Wybrano status: Bezwypadkowy");
        } else {
        }
    }

    @Step("Setting location")
    public void searchLocation(String voivodeship) {
        while (helper.swipeToElementByText("Wybierz lokalizację")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        btn_chooseLocation.click();
        System.out.println("Wybrano zakładkę \"Wybierz lokalizację\" ");
        while (helper.swipeToElementByText(voivodeship)) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement chooseVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + voivodeship + "')]"));
        chooseVoivodeship.click();
        System.out.println("Wybrano województwo: " + voivodeship);
        helper.waitTime(2);

        System.out.println("Wybrano zakładkę \"Wybierz lokalizację\" ");
        WebElement chooseAllVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Całe " + voivodeship + "')]"));
        chooseAllVoivodeship.click();
    }

    @Step("Setting results")
    public void searchShowResults() {
        WebElement showResults = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pokaż wyniki" + "')]"));
        showResults.click();
    }

    @Step("Setting vehicle type")
    public void searchVehicleType(String vehicleType) {
        while (helper.swipeToElementByText("Rodzaj Pojazdu")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }
        WebElement chooseVehicleType = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Rodzaj Pojazdu" + "')]"));
        chooseVehicleType.click();
        System.out.println("Wybrano zakładkę \"Rodzaj pojazdu\"");
        input_vehicleType.sendKeys(vehicleType);
        btn_chooseType.click();
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        System.out.println("Wybrano: " + vehicleType);
        helper.waitTime(3);

    }
    //endregion METHODS


}
