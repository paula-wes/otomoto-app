package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class SearchTrucksScreen extends BaseTest {

    //region BUTTONS

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_application;

    @FindBy(how = How.ID, using = "pl.otomoto:id/show_more_filters")
    public WebElement btn_moreParameters;

    @FindBy(how = How.ID, using = "pl.otomoto:id/filter")
    public WebElement input_vehicleBrand;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseBrand;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_text_value")
    public WebElement input_vehicleModel;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_countryOfOrigin;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseCountry;

    @FindBy(how = How.ID, using = "pl.otomoto:id/chooserBtn")
    public WebElement btn_chooseLocation;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_addEquip;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseAddEquip;

    @FindBy(how = How.CLASS_NAME, using = "android.widget.EditText")
    public List<WebElement> list_data;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_fuel;

    @FindBy(how = How.ID, using ="android:id/text1")
    public WebElement btn_chooseFuel;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_min")
    public WebElement input_priceMin;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_max")
    public WebElement input_priceMax;

    @FindBy(how = How.ID, using = "pl.otomoto:id/switch_value")
    public WebElement switch_dieselFilter;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_text_value")
    public WebElement input_range;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_min")
    public WebElement input_yearProdFrom;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_max")
    public WebElement input_yearProdTo;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_min")
    public List<WebElement> input_min;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_max")
    public List<WebElement> input_max;



    //endregion BUTTONS



    //region METHODS

    MethodHelper helper = new MethodHelper();
    WebDriver driver = getDriver();

    @Step("Setting trucks category")
    public void searchSetTrucks() {

        helper.waitTime(3);

        while (helper.swipeToElementByText("CIĘŻAROWE")) {
            new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point(984, 312))
                    .moveTo(PointOption.point(108, 316)).release().perform();  }

        helper.waitTime(5);
        WebElement trucks_cat = getDriver().findElement(By.xpath("//*[contains(@text, '" + "CIĘŻAROWE" + "')]"));
        trucks_cat.click();
        System.out.println("Wybrano zakładkę \"Ciężarowe\"");
    }
    @Step("Setting application")
    public void searchApplication(String application) {
        btn_application.click();
        System.out.println("Wybrano zakładkę \"Zastosowanie\"");

        while (helper.swipeToElementByText(application)) {
            new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point(176, 2131))
                    .moveTo(PointOption.point(318, 1000)).release().perform();  }
        WebElement applications = getDriver().findElement(By.xpath("//*[contains(@text, '" + application + "')]"));
        applications.click();
        System.out.println("Wybrano: " + application);
    }

    @Step("Setting model of vehicle")
    public void searchVehicleModel( String vehicleModel) {
        while (helper.swipeToElementByText("Model pojazdu")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }
        WebElement setVehicleModel = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Model pojazdu" + "')]"));
        setVehicleModel.click();
        input_vehicleModel.sendKeys(vehicleModel);
        helper.pressAndroidBackBtn();
        System.out.println("Wpisano model pojazdu: " + vehicleModel);
        helper.waitTime(3);

    }

    @Step("Setting brand of vehicle")
    public void searchVehicleBrand(String vehicleBrand) {
        while (helper.swipeToElementByText("Marka pojazdu")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }
        WebElement chooseVehicleBrand = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Marka pojazdu" + "')]"));
        chooseVehicleBrand.click();
        System.out.println("Wybrano zakładkę \"Marka pojazdu\"");
        input_vehicleBrand.sendKeys(vehicleBrand);
        btn_chooseBrand.click();
        System.out.println("Wybrano: " + vehicleBrand);
        helper.waitTime(3);

    }
    @Step("Setting min and max price")
    public void searchSetPrice(String priceMin, String priceMax) {
        while (helper.swipeToElementByText("Cena")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }

        for (int d = 0; d < input_max.size(); d++) {
            if (d == 1) {
                input_min.get(0).sendKeys(priceMin);
                input_max.get(0).sendKeys(priceMax);
            }
        }

        System.out.println("Wybrano cenę minimalną: " + priceMin);
        System.out.println("Wybrano cenę maksymalną: " + priceMax);
    }
    @Step("Setting privacy")
    public void searchSetPrivacy(String privacy) {
        while (helper.swipeToElementByText("Prywatnie lub firma")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        WebElement privateOrCorpo = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Prywatnie lub firma" + "')]"));
        privateOrCorpo.click();
        System.out.println("Wybrano zakładkę \"Prywatne lub firma\"");
        WebElement setPrivacy = getDriver().findElement(By.xpath("//*[contains(@text, '" + privacy + "')]"));
        setPrivacy.click();
        System.out.println("Wybrano: " + privacy);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
    }

    @Step("Setting currency")
    public void searchSetCurrency(String currency) {
        while (helper.swipeToElementByText("Waluta")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        WebElement currencies = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Waluta" + "')]"));
        currencies.click();
        System.out.println("Wybrano zakładkę \"Waluta\"");
        WebElement setCurrency = getDriver().findElement(By.xpath("//*[contains(@text, '" + currency + "')]"));
        setCurrency.click();
        System.out.println("Wybrano: " + currency);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting mileage")
    public void searchSetMileage(int nineteenth, int twentieth, String mileageMin, String mileageMax) {
        while (helper.swipeToElementByText("Przebieg")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == nineteenth) {
                list_data.get(nineteenth).sendKeys(mileageMin);
                list_data.get(twentieth).sendKeys(mileageMax);
            }
        }
        System.out.println("Wybrano minimalny przebieg: " + mileageMin);
        System.out.println("Wybrano maksymalny przebieg: " + mileageMax);
        helper.waitTime(3);
    }
    @Step("Setting min and max year of production")
    public void searchSetYearOfProduction(int third, int fourth, String yearOfProdFrom, String yearOfProdTo) {
        while (helper.swipeToElementByText("Rok produkcji")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < input_max.size(); d++) {
            if (d == 1) {
                input_min.get(1).sendKeys(yearOfProdFrom);
                input_max.get(1).sendKeys(yearOfProdTo);
            }
        }
        System.out.println("Wybrano rok produkcji od: " + yearOfProdFrom);
        System.out.println("Wybrano rok produkcji do: " + yearOfProdTo);
        helper.waitTime(3);
    }

    @Step("Setting country of origin")
    public void searchSetCountryOfOrigin(String country) {
        while (helper.swipeToElementByText("Kraj pochodzenia")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement chooseCountry = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Kraj pochodzenia" + "')]"));
        chooseCountry.click();
        System.out.println("Wybrano zakładkę \"Kraj pochodzenia\"");
        input_countryOfOrigin.sendKeys(country);
        btn_chooseCountry.click();
        System.out.println("Wybrano kraj: " + country);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting more parameters")
    public void searchMoreParameters() {
        while (helper.swipeToElementByText("WIĘCEJ PARAMETRÓW")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        btn_moreParameters.click();
        System.out.println("Wybrano zakładkę \"Więcej parametrów\" ");
    }

    @Step("Setting type of fuel")
    public void searchSetTypeOfFuel(String fuel) {
        while (helper.swipeToElementByText("Rodzaj paliwa")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        System.out.println("Otworzono zakładkę \"Rodzaj paliwa\"");
        WebElement chooseSystem = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Rodzaj paliwa" + "')]"));
        chooseSystem.click();
        input_fuel.sendKeys(fuel);
        btn_chooseFuel.click();
        System.out.println("Wybrano: " + fuel);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting min and max cubic capacity")
    public void searchSetCubicCapacity(int twentyFirst, int twentySecond, String cubicCapacityMin, String cubicCapacityMax) {
        while (helper.swipeToElementByText("Pojemność skokowa")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == twentyFirst) {
                list_data.get(twentyFirst).sendKeys(cubicCapacityMin);
                list_data.get(twentySecond).sendKeys(cubicCapacityMax);
            }
        }
        System.out.println("Wybrano minimalną pojemność skokową: " + cubicCapacityMin);
        System.out.println("Wybrano maksymalną pojemność skokową: " + cubicCapacityMax);
        helper.waitTime(3);
    }

    @Step("Setting min and max power")
    public void searchSetPower(int fifth, int sixth, String powerMin, String powerMax) {
        while (helper.swipeToElementByText("Moc")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == fifth) {
                list_data.get(fifth).sendKeys(powerMin);
                list_data.get(sixth).sendKeys(powerMax);
            }
        }
        System.out.println("Wybrano moc minimalną: " + powerMin);
        System.out.println("Wybrano moc maksymalną: " + powerMax);
        helper.waitTime(3);
    }
    @Step("Setting gearbox")
    public void searchSetGearbox(String gearbox) {
        while (helper.swipeToElementByText("Skrzynia biegów")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement gearboxes = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Skrzynia biegów" + "')]"));
        gearboxes.click();
        System.out.println("Wybrano zakładkę \"Skrzynia biegów\"");
        WebElement setGearbox = getDriver().findElement(By.xpath("//*[contains(@text, '" + gearbox + "')]"));
        setGearbox.click();
        System.out.println("Wybrano: " + gearbox);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }
    @Step("Setting  diesel particulate filter")
    public void searchSetDieselParticulateFilter(boolean dieselPartFilter) {
        while (helper.swipeToElementByText("Filtr cząstek stałych")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        if (dieselPartFilter) {
            switch_dieselFilter.click();
        }else{}

    }
    @Step("Setting additional equipment")
    public void searchSetAdditionalEquipment(String addEquip) {
        while (helper.swipeToElementByText("Dodatkowe wyposażenie")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement addEquips = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Dodatkowe wyposażenie" + "')]"));
        addEquips.click();
        System.out.println("Wybrano zakładkę \"Dodatkowe wyposażenie\"");
        input_addEquip.sendKeys(addEquip);
        btn_chooseAddEquip.click();
        System.out.println("Wybrano: " + addEquip);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting steering wheel on the right")
    public void searchSetSteeringWheel(String steereingWheel) {
        while (helper.swipeToElementByText("Kierownica po prawej (Anglik)")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement steereingWheels = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Kierownica po prawej (Anglik)" + "')]"));
        steereingWheels.click();
        System.out.println("Wybrano zakładkę \"Kierownica po prawej (Anglik)\"");
        WebElement setGearbox = getDriver().findElement(By.xpath("//*[contains(@text, '" + steereingWheel + "')]"));
        setGearbox.click();
        System.out.println("Wybrano: " + steereingWheel);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting colour")
    public void searchSetColour(String colour) {
        while (helper.swipeToElementByText("Kolor")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement colours = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Kolor" + "')]"));
        colours.click();
        System.out.println("Wybrano zakładkę \"Kolor\"");
        WebElement setGearbox = getDriver().findElement(By.xpath("//*[contains(@text, '" + colour + "')]"));
        setGearbox.click();
        System.out.println("Wybrano: " + colour);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting number of axles")
    public void searchSetNumberOfAxles( int ninth, int tenth, String axlesMin, String axlesMax) {
        while (helper.swipeToElementByText("Liczba osi")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == ninth) {
                list_data.get(ninth).sendKeys(axlesMin);
                list_data.get(tenth).sendKeys(axlesMax);
            }
        }
        System.out.println("Wybrano liczbę osi od: " + axlesMin);
        System.out.println("Wybrano liczbę osi do: " + axlesMax);
        helper.waitTime(3);

    }

    @Step("Setting permissible gross weight")
    public void searchSetPermissibleGrossWeight( int twentyThird, int twentyFourth, String permissibleWeightMin, String permissibleWeightMax) {
        while (helper.swipeToElementByText("Dopuszczalna masa całkowita")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == twentyThird) {
                list_data.get(twentyThird).sendKeys(permissibleWeightMin);
                list_data.get(twentyFourth).sendKeys(permissibleWeightMax);
            }
        }
        System.out.println("Wybrano minimalną dopuszczalną masę całkowitą: " + permissibleWeightMin);
        System.out.println("Wybrano maksymalną dopuszczalną masę całkowitą: " + permissibleWeightMax);
        helper.waitTime(3);

    }

    @Step("Setting permissible gross weight v2")
    public void searchSetBuggyWeight(int twentyFifth, int twentySixth, String minBuggyWeight, String maxBuggyWeight) {
        while (helper.swipeToElementByText("Dopuszczalna masa całkowita")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == twentyFifth) {
                list_data.get(twentyFifth).sendKeys(minBuggyWeight);
                list_data.get(twentySixth).sendKeys(maxBuggyWeight);
            }
        }
        System.out.println("Wybrano minimalną dopuszczalną masę całkowitą v2: " + minBuggyWeight);
        System.out.println("Wybrano maksymalną dopuszczalną masę całkowitą v2: " + maxBuggyWeight);
        helper.waitTime(3);

    }

    @Step("Setting non-standard vehicle")
    public void searchSetNonStandardVehicle(boolean nonStandardVec) {
        while (helper.swipeToElementByText("Pojazd nienormatywny")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        if (nonStandardVec) {
            WebElement nonStandardVehicles = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pojazd nienormatywny" + "')]"));
            nonStandardVehicles.click();
        }else{}

    }


    @Step("Setting cooling aggregate")
    public void searchSetCoolingAggregate(boolean coolAggregate) {
        while (helper.swipeToElementByText("Agregat chłodzący")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        if (coolAggregate) {
            WebElement setCoolAggregate = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Agregat chłodzący" + "')]"));
            setCoolAggregate.click();
        }else{}

    }

    @Step("Setting double rear wheels")
    public void searchSetDoubleRearWheels(boolean rearWheels) {
        while (helper.swipeToElementByText("Tylne koła podwójne")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        if (rearWheels) {
            WebElement doubleRearWheels = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Tylne koła podwójne" + "')]"));
            doubleRearWheels.click();
        }else{}

    }

    @Step("Setting min and max capacity")
    public void searchSetCapacity(int seventh, int eighth, String capacityMin, String capacityMax) {
        while (helper.swipeToElementByText("Pojemność")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == seventh) {
                list_data.get(seventh).sendKeys(capacityMin);
                list_data.get(eighth).sendKeys(capacityMax);
            }
        }
        System.out.println("Wybrano pojemność od: " + capacityMin);
        System.out.println("Wybrano pojemność do: " + capacityMax);
        helper.waitTime(3);
    }

    @Step("Setting financial information")
    public void searchFinancialInformation(boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                           boolean financingOption, boolean leasing, boolean rent) {

        System.out.println("Wybrano zakładkę \"Informacje finansowe\"");
        while (helper.swipeToElementByText("Możliwość finansowania")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }

        if (authorizedDealer) {
            WebElement setAuthorizedDealer = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Autoryzowanego Dealera" + "')]"));
            setAuthorizedDealer.click();
            System.out.println("Wybrano leasing");
        } else {
        }
        if (vatMargin) {
            WebElement setVatMargin = getDriver().findElement(By.xpath("//*[contains(@text, '" + "VAT marża" + "')]"));
            setVatMargin.click();
            System.out.println("Wybrano opcję: VAT marża");
        } else {
        }
        if (invoiceVat) {
            WebElement setInvoiceVat = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Faktura VAT" + "')]"));
            setInvoiceVat.click();
            System.out.println("Wybrano opcję: faktura VAT");
        } else {
        }
        if (financingOption) {
            WebElement setFinancingOption = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Możliwość finansowania" + "')]"));
            setFinancingOption.click();
            System.out.println("Wybrano możliwość finansowania");
        } else {
        }

        if (leasing) {
            WebElement setLeasing = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Leasing" + "')]"));
            setLeasing.click();
            System.out.println("Wybrano leasing");
        } else {
        }
        if (rent) {
            WebElement setRent = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Opcja wynajem" + "')]"));
            setRent.click();
            System.out.println("Wybrano możliwość wynajmu");
        } else {
        }
    }

    @Step("Setting car status")
    public void searchCarStatus(boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                boolean noAccidents, boolean servisedASO) {

        System.out.println("Wybrano zakładkę \"Status pojazdu\"");
        while (helper.swipeToElementByText("Bezwypadkowy")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }

        if (registrationNumber) {
            WebElement setRegistrationNumber = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Ma numer rejestracyjny" + "')]"));
            setRegistrationNumber.click();
            System.out.println("Wybrano status: Ma numer rejestracyjny");
        } else {
        }
        if (offersWithVin) {
            WebElement setOffersWithWin = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pokaż oferty z numerem VIN" + "')]"));
            setOffersWithWin.click();
            System.out.println("Wybrano status: Pokaż oferty z numerem VIN");
        } else {
        }
        if (damaged) {
            WebElement setDamaged = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Uszkodzony" + "')]"));
            setDamaged.click();
            System.out.println("Wybrano status: Uszkodzony");
        } else {
        }
        if (registerInPoland) {
            WebElement setRegisterInPoland = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Zarejestrowany w Polsce" + "')]"));
            setRegisterInPoland.click();
            System.out.println("Wybrano status: Zarejestrowany w Polsce");
        } else {
        }
        if (noAccidents) {
            WebElement setNoAccidents = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Bezwypadkowy" + "')]"));
            setNoAccidents.click();
            System.out.println("Wybrano status: Bezwypadkowy");
        } else {
        }
        if (servisedASO) {
        WebElement setServisedASO = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Serwisowany w ASO" + "')]"));
            setServisedASO.click();
        System.out.println("Wybrano status: Serwisowany w ASO");
    } else {}
    }

    @Step("Setting location")
    public void searchLocation(String voivodeship) {
        while (helper.swipeToElementByText("Wybierz lokalizację")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        btn_chooseLocation.click();
        System.out.println("Wybrano zakładkę \"Wybierz lokalizację\" ");
        while (helper.swipeToElementByText(voivodeship)) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement chooseVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + voivodeship + "')]"));
        chooseVoivodeship.click();
        System.out.println("Wybrano województwo: " + voivodeship);
        helper.waitTime(2);

        System.out.println("Wybrano zakładkę \"Wybierz lokalizację\" ");
        WebElement chooseAllVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Całe " + voivodeship + "')]"));
        chooseAllVoivodeship.click();
    }

    @Step("Setting min and max capacity / volume")
    public void searchSetCapacityCap(int twentySeventh, int twentyEighth, String capacityCapMin, String capacityCapMax) {
        while (helper.swipeToElementByText("Pojemność (objętość)")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == twentySeventh) {
                list_data.get(twentySeventh).sendKeys(capacityCapMin);
                list_data.get(twentyEighth).sendKeys(capacityCapMax);
            }
        }
        System.out.println("Wybrano pojemność/objętość od: " + capacityCapMin);
        System.out.println("Wybrano pojemność/objętość do: " + capacityCapMax);
        helper.waitTime(3);
    }

    @Step("Setting min and max allowed package")
    public void searchSetAllowedPackage(int twentyNinth, int thirtieth, String packageMin, String packageCapMax) {
        while (helper.swipeToElementByText("Dopuszczalna ładowność")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == twentyNinth) {
                list_data.get(twentyNinth).sendKeys(packageMin);
                list_data.get(thirtieth).sendKeys(packageCapMax);
            }
        }
        System.out.println("Wybrano minimalną dopuszczalną ładowność: " + packageMin);
        System.out.println("Wybrano maksymalną dopuszczalną ładowność: " + packageCapMax);
        helper.waitTime(3);
    }

    @Step("Setting results")
    public void searchShowResults() {
        WebElement showResults = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pokaż wyniki" + "')]"));
        showResults.click();
    }

    @Step("Setting range")
    public void searchSetRange(String range) {
        while (helper.swipeToElementByText("Zasięg")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement setVehicleModel = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Zasięg" + "')]"));
        setVehicleModel.click();
        input_range.sendKeys(range);
        helper.pressAndroidBackBtn();
        System.out.println("Wpisano zasięg: " + range);
        helper.waitTime(3);
    }


    //endregion METHODS





}
