package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;


public class RegistrationScreen extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @FindBy(how = How.ID, using = "com.google.android.gms:id/cancel")
    public WebElement btn_cancelSmartLock;
    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@content-desc=\"Otwórz menu\"]")
    public WebElement btn_openMenu;
    @FindBy(how = How.ID, using = "pl.otomoto:id/navigation_view_header_profile_name")
    public WebElement btn_log;
    @FindBy(how = How.ID, using = "pl.otomoto:id/txtRegister")
    public WebElement btn_goToRegister;
    @FindBy(how = How.CLASS_NAME, using = "android.widget.EditText")
    public List<WebElement> list_data;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wyrażam')]")
    public WebElement btn_checkbox;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Utwórz')]")
    public WebElement btn_register;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wróć na')]")
    public WebElement btn_backTolog;
    public void regGoToRegister() {
        btn_cancelSmartLock.click();
        methodHelper.waitTime(1);
        btn_openMenu.click();
        btn_log.click();
        btn_goToRegister.click();
    }

    public void regInputData(int first, int second, int third, String email, String password, String repeatPassword) {

        methodHelper.waitTime(2);
        for (int d = 0; d < list_data.size(); d++) {
            if (d == first) {
                list_data.get(first).sendKeys(email);
                list_data.get(second).sendKeys(password);
                list_data.get(third).sendKeys(repeatPassword);
            }
        }

    }
    public void regCheckbox(){
        btn_checkbox.click();
    }
    public void regRegister(){

        methodHelper.scrollText("Utwórz", direction.UP, "up", 0.7, 1);
        methodHelper.waitTime(1);
        btn_register.click();
    }
    public void regBackToLog(){
        btn_backTolog.click();
    }
}