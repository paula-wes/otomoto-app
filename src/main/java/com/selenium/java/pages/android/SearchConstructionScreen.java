package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class SearchConstructionScreen extends BaseTest {

    //region BUTTONS

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_application;

    @FindBy(how = How.ID, using = "pl.otomoto:id/show_more_filters")
    public WebElement btn_moreParameters;

    @FindBy(how = How.ID, using = "pl.otomoto:id/filter")
    public WebElement input_vehicleBrand;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseBrand;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_text_value")
    public WebElement input_vehicleModel;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_countryOfOrigin;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseCountry;

    @FindBy(how = How.ID, using = "pl.otomoto:id/chooserBtn")
    public WebElement btn_chooseLocation;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_addEquip;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseAddEquip;

    @FindBy(how = How.CLASS_NAME, using = "android.widget.EditText")
    public List<WebElement> list_data;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_system;

    @FindBy(how = How.ID, using ="android:id/text1")
    public WebElement btn_chooseSystem;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_fuel;

    @FindBy(how = How.ID, using ="android:id/text1")
    public WebElement btn_chooseFuel;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_min")
    public WebElement input_priceMin;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_max")
    public WebElement input_priceMax;



    //endregion BUTTONS

    //region METHODS

    MethodHelper helper = new MethodHelper();
    WebDriver driver = getDriver();

    @Step("Setting constructions category")
    public void searchSetConstructions() {

        helper.waitTime(3);

        while (helper.swipeToElementByText("BUDOWLANE")) {
            new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point(984, 312))
                    .moveTo(PointOption.point(108, 316)).release().perform();  }

        helper.waitTime(5);
        WebElement agricultural_cat = getDriver().findElement(By.xpath("//*[contains(@text, '" + "BUDOWLANE" + "')]"));
        agricultural_cat.click();
        System.out.println("Wybrano zakładkę \"Budowlane\"");
    }

    @Step("Setting application")
    public void searchApplication(String application) {
        btn_application.click();
        System.out.println("Wybrano zakładkę \"Zastosowanie\"");
        while (helper.swipeToElementByText(application)) {
            new TouchAction((PerformsTouchActions) driver).longPress(PointOption.point(172, 2136))
                    .moveTo(PointOption.point(204, 1000)).release().perform(); }
        WebElement applications = getDriver().findElement(By.xpath("//*[contains(@text, '" + application + "')]"));
        applications.click();
        System.out.println("Wybrano: " + application);
    }

    @Step("Setting model of vehicle")
    public void searchVehicleModel( String vehicleModel) {
        while (helper.swipeToElementByText("Model pojazdu")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }
        WebElement setVehicleModel = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Model pojazdu" + "')]"));
        setVehicleModel.click();
        input_vehicleModel.sendKeys(vehicleModel);
        helper.pressAndroidBackBtn();
        System.out.println("Wpisano model pojazdu: " + vehicleModel);
        helper.waitTime(3);

    }

    @Step("Setting brand of vehicle")
    public void searchVehicleBrand(String vehicleBrand) {
        while (helper.swipeToElementByText("Marka pojazdu")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }
        WebElement chooseVehicleBrand = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Marka pojazdu" + "')]"));
        chooseVehicleBrand.click();
        System.out.println("Wybrano zakładkę \"Marka pojazdu\"");
        input_vehicleBrand.sendKeys(vehicleBrand);
        btn_chooseBrand.click();
        System.out.println("Wybrano: " + vehicleBrand);
        helper.waitTime(3);

    }
    @Step("Setting min and max price")
    public void searchSetPrice(String priceMin, String priceMax) {
        while (helper.swipeToElementByText("Cena")) {
            helper.swipeInDirection(direction.UP, "up", 0.2);
        }
        input_priceMin.sendKeys(priceMin);
        System.out.println("Wybrano cenę minimalną: " + priceMin);
        input_priceMax.sendKeys(priceMax);
        System.out.println("Wybrano cenę maksymalną: " + priceMax);
    }
    @Step("Setting privacy")
    public void searchSetPrivacy(String privacy) {
        WebElement privateOrCorpo = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Prywatnie lub firma" + "')]"));
        privateOrCorpo.click();
        System.out.println("Wybrano zakładkę \"Prywatne lub firma\"");
        WebElement setPrivacy = getDriver().findElement(By.xpath("//*[contains(@text, '" + privacy + "')]"));
        setPrivacy.click();
        System.out.println("Wybrano: " + privacy);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
    }

    @Step("Setting currency")
    public void searchSetCurrency(String currency) {
        while (helper.swipeToElementByText("Waluta")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        WebElement currencies = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Waluta" + "')]"));
        currencies.click();
        System.out.println("Wybrano zakładkę \"Waluta\"");
        WebElement setCurrency = getDriver().findElement(By.xpath("//*[contains(@text, '" + currency + "')]"));
        setCurrency.click();
        System.out.println("Wybrano: " + currency);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }
    @Step("Setting min and max year of production")
    public void searchSetYearOfProduction(int third, int fourth, String yearOfProdFrom, String yearOfProdTo) {
        while (helper.swipeToElementByText("Kraj pochodzenia")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == third) {
                list_data.get(third).sendKeys(yearOfProdFrom);
                list_data.get(fourth).sendKeys(yearOfProdTo);
            }
        }
        System.out.println("Wybrano rok produkcji od: " + yearOfProdFrom);
        System.out.println("Wybrano rok produkcji do: " + yearOfProdTo);
        helper.waitTime(3);
    }

    @Step("Setting country of origin")
    public void searchSetCountryOfOrigin(String country) {
        while (helper.swipeToElementByText("Kraj pochodzenia")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement chooseCountry = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Kraj pochodzenia" + "')]"));
        chooseCountry.click();
        System.out.println("Wybrano zakładkę \"Kraj pochodzenia\"");
        input_countryOfOrigin.sendKeys(country);
        btn_chooseCountry.click();
        System.out.println("Wybrano kraj: " + country);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting more parameters")
    public void searchMoreParameters() {
        while (helper.swipeToElementByText("WIĘCEJ PARAMETRÓW")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        btn_moreParameters.click();
        System.out.println("Wybrano zakładkę \"Więcej parametrów\" ");
    }

    @Step("Setting financial information")
    public void searchFinancialInformation(boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                           boolean financingOption, boolean leasing, boolean rent) {

        System.out.println("Wybrano zakładkę \"Informacje finansowe\"");
        while (helper.swipeToElementByText("VAT marża")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }

        if (authorizedDealer) {
            WebElement setAuthorizedDealer = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Autoryzowanego Dealera" + "')]"));
            setAuthorizedDealer.click();
            System.out.println("Wybrano leasing");
        } else {
        }
        if (vatMargin) {
            WebElement setVatMargin = getDriver().findElement(By.xpath("//*[contains(@text, '" + "VAT marża" + "')]"));
            setVatMargin.click();
            System.out.println("Wybrano opcję: VAT marża");
        } else {
        }
        while (helper.swipeToElementByText("Opcja wynajem")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        if (invoiceVat) {
            WebElement setInvoiceVat = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Faktura VAT" + "')]"));
            setInvoiceVat.click();
            System.out.println("Wybrano opcję: faktura VAT");
        } else {
        }
        if (financingOption) {
            WebElement setFinancingOption = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Możliwość finansowania" + "')]"));
            setFinancingOption.click();
            System.out.println("Wybrano możliwość finansowania");
        } else {
        }


        if (leasing) {
            WebElement setLeasing = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Leasing" + "')]"));
            setLeasing.click();
            System.out.println("Wybrano leasing");
        } else {
        }

        if (rent) {
            WebElement setRent = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Opcja wynajem" + "')]"));
            setRent.click();
            System.out.println("Wybrano możliwość wynajmu");
        } else {
        }
    }

    @Step("Setting car status")
    public void searchCarStatus(boolean damaged, boolean registerInPoland, boolean firstOwner,
                                boolean noAccidents) {

        System.out.println("Wybrano zakładkę \"Status pojazdu\"");
        while (helper.swipeToElementByText("Bezwypadkowy")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }

        if (damaged) {
            WebElement setDamaged = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Uszkodzony" + "')]"));
            setDamaged.click();
            System.out.println("Wybrano status: Uszkodzony");
        } else {
        }
        if (registerInPoland) {
            WebElement setRegisterInPoland = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Zarejestrowany w Polsce" + "')]"));
            setRegisterInPoland.click();
            System.out.println("Wybrano status: Zarejestrowany w Polsce");
        } else {
        }
        if (firstOwner) {
            WebElement setFirstOwner = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pierwszy właściciel" + "')]"));
            setFirstOwner.click();
            System.out.println("Wybrano status: Pierwszy właściciel");
        } else {
        }
        if (noAccidents) {
            WebElement setNoAccidents = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Bezwypadkowy" + "')]"));
            setNoAccidents.click();
            System.out.println("Wybrano status: Bezwypadkowy");
        } else {
        }
    }

    @Step("Setting location")
    public void searchLocation(String voivodeship) {
        while (helper.swipeToElementByText("Wybierz lokalizację")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        btn_chooseLocation.click();
        System.out.println("Wybrano zakładkę \"Wybierz lokalizację\" ");
        while (helper.swipeToElementByText(voivodeship)) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        WebElement chooseVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + voivodeship + "')]"));
        chooseVoivodeship.click();
        System.out.println("Wybrano województwo: " + voivodeship);
        helper.waitTime(2);

        System.out.println("Wybrano zakładkę \"Wybierz lokalizację\" ");
        WebElement chooseAllVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Całe " + voivodeship + "')]"));
        chooseAllVoivodeship.click();
    }

    @Step("Setting results")
    public void searchShowResults() {
        WebElement showResults = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pokaż wyniki" + "')]"));
        showResults.click();

    }

    @Step("Setting min and max power")
    public void searchSetPower(int fifth, int sixth, String powerMin, String powerMax) {
        while (helper.swipeToElementByText("Moc")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == fifth) {
                list_data.get(fifth).sendKeys(powerMin);
                list_data.get(sixth).sendKeys(powerMax);
            }
        }
        System.out.println("Wybrano moc minimalną: " + powerMin);
        System.out.println("Wybrano moc maksymalną: " + powerMax);
        helper.waitTime(3);
    }

    @Step("Setting additional equipment")
    public void searchSetAdditionalEquipment(String addEquip) {
        while (helper.swipeToElementByText("Dodatkowe wyposażenie")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        WebElement addEquips = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Dodatkowe wyposażenie" + "')]"));
        addEquips.click();
        System.out.println("Wybrano zakładkę \"Dodatkowe wyposażenie\"");
        input_addEquip.sendKeys(addEquip);
        btn_chooseAddEquip.click();
        System.out.println("Wybrano: " + addEquip);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting running system")
    public void searchSetRunningSystem(String system) {
        while (helper.swipeToElementByText("Układ bieżny")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        System.out.println("Otworzono zakładkę \"Nadwozie\"");
        WebElement chooseSystem = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Układ bieżny" + "')]"));
        chooseSystem.click();
        System.out.println("Wybrano zakładkę \"Układ bieżny\"");
        input_system.sendKeys(system);
        btn_chooseSystem.click();
        System.out.println("Wybrano: " + system);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting min and max capacity")
    public void searchSetCapacity(int seventh, int eighth, String capacityMin, String capacityMax) {
        while (helper.swipeToElementByText("Ładowność")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == seventh) {
                list_data.get(seventh).sendKeys(capacityMin);
                list_data.get(eighth).sendKeys(capacityMax);
            }
        }
        System.out.println("Wybrano ładowność od: " + capacityMin);
        System.out.println("Wybrano ładowność do: " + capacityMax);
        helper.waitTime(3);
    }
    @Step("Setting min and max bucket capacity")
    public void searchSetBucketCapacity(int ninth, int tenth, String bucketCapacityMin, String bucketCapacityMax) {
        while (helper.swipeToElementByText("Pojemność łyżki")) {
            helper.swipeInDirection(direction.UP, "up", 0.4);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == ninth) {
                list_data.get(ninth).sendKeys(bucketCapacityMin);
                list_data.get(tenth).sendKeys(bucketCapacityMax);
            }
        }
        System.out.println("Wybrano pojemność łyżki od: " + bucketCapacityMin);
        System.out.println("Wybrano pojemność łyżki do: " + bucketCapacityMax);
        helper.waitTime(3);
    }

    @Step("Setting min and max digging depth")
    public void searchSetDiggingDepth(int eleventh, int twelfth, String depthMin, String depthMax) {
        while (helper.swipeToElementByText("Głębokośc kopania")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == eleventh) {
                list_data.get(eleventh).sendKeys(depthMin);
                list_data.get(twelfth).sendKeys(depthMax);
            }
        }
        System.out.println("Wybrano głębokość kopania od: " + depthMin);
        System.out.println("Wybrano głębokość kopania do: " + depthMax);
        helper.waitTime(3);
    }

    @Step("Setting lifting capacity")
    public void searchSetLiftingCapacity(int thirteenth, String liftingCapacity) {
        while (helper.swipeToElementByText("Udźwig")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == thirteenth) {
                list_data.get(thirteenth).sendKeys(liftingCapacity);
            }
        }
        System.out.println("Wybrano udźwig: " + liftingCapacity);
        helper.waitTime(3);
    }

    @Step("Setting lifting height")
    public void searchSetLiftingHeight(int fourteenth, String liftingHeight) {
        while (helper.swipeToElementByText("Wysokość podnoszenia")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == fourteenth) {
                list_data.get(fourteenth).sendKeys(liftingHeight);
            }
        }
        System.out.println("Wybrano wysokość podnoszenia: " + liftingHeight);
        helper.waitTime(3);
    }

    @Step("Setting type of fuel")
    public void searchSetTypeOfFuel(String fuel) {
        while (helper.swipeToElementByText("Rodzaj paliwa")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        System.out.println("Otworzono zakładkę \"Rodzaj paliwa\"");
        WebElement chooseSystem = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Rodzaj paliwa" + "')]"));
        chooseSystem.click();
        input_fuel.sendKeys(fuel);
        btn_chooseFuel.click();
        System.out.println("Wybrano: " + fuel);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting capacitance")
    public void searchSetCapacitance(int fifteenth, int sixteenth, String capacitanceMin, String capacistanceMax) {
        while (helper.swipeToElementByText("Pojemność")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == fifteenth) {
                list_data.get(fifteenth).sendKeys(capacitanceMin);
                list_data.get(sixteenth).sendKeys(capacistanceMax);
            }
        }
        System.out.println("Wybrano pojemność minimalną: " + capacitanceMin);
        System.out.println("Wybrano pojemność maksymalną: " + capacistanceMax);
        helper.waitTime(3);
    }

    @Step("Setting gearbox")
    public void searchSetGearbox(String gearbox) {
        while (helper.swipeToElementByText("Skrzynia biegów")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        WebElement gearboxes = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Skrzynia biegów" + "')]"));
        gearboxes.click();
        System.out.println("Wybrano zakładkę \"Skrzynia biegów\"");
        WebElement setGearbox = getDriver().findElement(By.xpath("//*[contains(@text, '" + gearbox + "')]"));
        setGearbox.click();
        System.out.println("Wybrano: " + gearbox);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting weight")
    public void searchSetWeight(int seventeenth, int eighteenth, String weightMin, String weightMax) {
        while (helper.swipeToElementByText("Masa")) {
            helper.swipeInDirection(direction.UP, "up", 0.3);
        }
        for (int d = 0; d < list_data.size(); d++) {
            if (d == seventeenth) {
                list_data.get(seventeenth).sendKeys(weightMin);
                list_data.get(eighteenth).sendKeys(weightMax);
            }
        }
        System.out.println("Wybrano masę minimalną: " + weightMin);
        System.out.println("Wybrano masę maksymalną: " + weightMax);
        helper.waitTime(3);
    }

    //endregion METHODS

}
