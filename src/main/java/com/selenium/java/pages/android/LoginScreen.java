package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;

import io.qameta.allure.Step;

import com.selenium.java.helper.MethodHelper;

import io.qameta.allure.Step;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginScreen extends BaseTest {


    MethodHelper methodHelper = new MethodHelper();

    //region BUTTONS

    @FindBy(how = How.XPATH, using = "//android.widget.ImageButton[@content-desc=\"Otwórz menu\"]")
    public WebElement btn_menu;

    @FindBy(how = How.ID, using = "pl.otomoto:id/navigation_view_header_profile_name")
    public WebElement btn_loginToApp;

    @FindBy(how = How.ID, using = "pl.otomoto:id/edtEmail")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.otomoto:id/edtPassword")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.otomoto:id/login_layout_btn_login_text")
    public WebElement btn_logIn;


    //endregion BUTTONS

    //region METHODS

    @Step("Setting email and password to log in")
    public void loginToApp(String email, String password){

        btn_menu.click();

        System.out.println("Moved to menu");
        btn_loginToApp.click();
        System.out.println("Clicked on login");
        input_email.sendKeys(email);
        System.out.println("Entered email: " + email);
        input_password.sendKeys(password);
        System.out.println("Entered password: " + password);
        getWaitDriver(4);
        btn_logIn.click();
        System.out.println("Cliked on button: \"Zaloguj\"");
    }
    //endregion BUTTONS
}



