package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchPartsScreen extends BaseTest {

    //region BUTTONS

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_application;

    @FindBy(how = How.ID, using = "pl.otomoto:id/show_more_filters")
    public WebElement btn_moreParameters;

    @FindBy(how = How.ID, using = "pl.otomoto:id/filter")
    public WebElement input_vehicleBrand;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_min")
    public WebElement input_priceMin;

    @FindBy(how = How.ID, using = "pl.otomoto:id/et_range_max")
    public WebElement input_priceMax;

    @FindBy(how = How.ID, using = "pl.otomoto:id/chooserBtn")
    public WebElement btn_chooseLocation;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseType;

    @FindBy(how = How.ID, using = "pl.otomoto:id/dialog_onboarding_filterable_single_choice_filter")
    public WebElement input_searchMotorcycleParts;

    @FindBy(how = How.ID, using = "android:id/text1")
    public WebElement btn_chooseBrand;

    //endregion BUTTONS

    //region METHODS

    MethodHelper helper = new MethodHelper();

    @Step("Setting parts category")
    public void searchSetParts(String parts) {
        WebElement parts_cat = getDriver().findElement(By.xpath("//*[contains(@text, '" + parts + "')]"));
        parts_cat.click();
        System.out.println("Wybrano zakładkę \"Części\"");
    }

    @Step("Setting application")
    public void searchApplication(String application) {
        btn_application.click();
        System.out.println("Wybrano zakładkę \"Zastosowanie\"");
        WebElement applications = getDriver().findElement(By.xpath("//*[contains(@text, '" + application + "')]"));
        applications.click();
    }

    @Step("Setting delivery")
    public void searchDelivery(String deliveryOption) {
        WebElement delivery = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Dostawa" + "')]"));
        delivery.click();
        System.out.println("Wybrano opcję dostawy");

        WebElement deliveryOptions = getDriver().findElement(By.xpath("//*[contains(@text, '" + deliveryOption + "')]"));
        deliveryOptions.click();
        System.out.println("Wybrano dostawę: " + deliveryOption);

        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
    }

    @Step("Setting more parameters")
    public void searchMoreParameters() {
        btn_moreParameters.click();
        System.out.println("Wybrano zakładkę \"Więcej parametrów\" ");
    }

    @Step("Setting privacy")
    public void searchSetPrivacy(String privacy) {
        WebElement privateOrCorpo = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Prywatnie lub firma" + "')]"));
        privateOrCorpo.click();
        System.out.println("Wybrano zakładkę \"Prywatne lub firma\"");
        WebElement setPrivacy = getDriver().findElement(By.xpath("//*[contains(@text, '" + privacy + "')]"));
        setPrivacy.click();
        System.out.println("Wybrano: " + privacy);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
    }

    @Step("Setting currency")
    public void searchSetCurrency(String currency) {
        while (helper.swipeToElementByText("Waluta")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        WebElement currencies = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Waluta" + "')]"));
        currencies.click();
        System.out.println("Wybrano zakładkę \"Waluta\"");
        WebElement setCurrency = getDriver().findElement(By.xpath("//*[contains(@text, '" + currency + "')]"));
        setCurrency.click();
        System.out.println("Wybrano: " + currency);
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
        helper.waitTime(3);
    }

    @Step("Setting brand of vehicle")
    public void searchVehicleBrand(String vehicleBrand) {
        while (helper.swipeToElementByText("Marka pojazdu")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        WebElement chooseVehicleBrand = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Marka pojazdu" + "')]"));
        chooseVehicleBrand.click();
        System.out.println("Wybrano zakładkę \"Marka pojazdu\"");
        input_vehicleBrand.sendKeys(vehicleBrand);
        btn_chooseBrand.click();
        System.out.println("Wybrano: " + vehicleBrand);
        helper.waitTime(3);

    }

    @Step("Setting type of parts")
    public void searchTypeOfParts(String partName) {
        while (helper.swipeToElementByText("Rodzaj części")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        WebElement chooseTypeOfParts = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Rodzaj części" + "')]"));
        chooseTypeOfParts.click();
        System.out.println("Wybrano zakładkę \"Rodzaj części\"");
        input_searchMotorcycleParts.sendKeys(partName);
//        btn_chooseType.click();
        WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
        ready.click();
    }


    @Step("Setting min and max price")
    public void searchSetPrice(String priceMin, String priceMax) {
        while (helper.swipeToElementByText("Cena")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        input_priceMin.sendKeys(priceMin);
        System.out.println("Wybrano cenę minimalną: " + priceMin);
        helper.waitTime(4);
        input_priceMax.sendKeys(priceMax);
        System.out.println("Wybrano cenę maksymalną: " + priceMax);
    }

    @Step("Setting location")
    public void searchLocation(String voivodeship) {
        while (helper.swipeToElementByText("Wybierz lokalizację")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        btn_chooseLocation.click();
        System.out.println("Wybrano zakładkę \"Wybierz lokalizację\" ");
        while (helper.swipeToElementByText(voivodeship)) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        WebElement chooseVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + voivodeship + "')]"));
        chooseVoivodeship.click();
        System.out.println("Wybrano województwo: " + voivodeship);
        helper.waitTime(2);

        System.out.println("Wybrano zakładkę \"Wybierz lokalizację\" ");
        WebElement chooseAllVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Całe " + voivodeship + "')]"));
        chooseAllVoivodeship.click();


//        WebElement chooseCity = getDriver().findElement(By.xpath("//*[contains(@text, '" + voivodeship + "')]"));
//        chooseCity.click();
//        System.out.println("Wybrano miasto: " + city);
//        helper.waitTime(2);}
    }

    @Step("Setting results")
    public void searchShowResults() {
        WebElement showResults = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Pokaż wyniki" + "')]"));
        showResults.click();
    }

    @Step("Setting profil")
    public void searchProfile(boolean profil, String profile) {
        while (helper.swipeToElementByText("Profil")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }

        if (profil) {
            WebElement profiles = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Profil" + "')]"));
            profiles.click();
            System.out.println("Wybrano zakładkę \"Profil\"");
            WebElement chooseProfile = getDriver().findElement(By.xpath("//*[contains(@text, '" + profile + "')]"));
            chooseProfile.click();
            System.out.println("Wybrano profil: " + profile);
            WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
            ready.click();
        } else {
        }
    }

    @Step("Setting type of rim")
    public void searchRimType(boolean rimtype, String rimType) {

        if (rimtype) {
            WebElement rimTypes = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Rodzaj felg" + "')]"));
            rimTypes.click();
            System.out.println("Wybrano zakładkę \"Rodzaj felg\"");
            WebElement chooseRimType = getDriver().findElement(By.xpath("//*[contains(@text, '" + rimType + "')]"));
            chooseRimType.click();
            System.out.println("Wybrano: " + rimType);
            WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
            ready.click();
        } else {
        }
    }

    @Step("Setting width")
    public void searchWidth(boolean szer, String width) {
        if (szer) {
            WebElement widths = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Szerokość" + "')]"));
            widths.click();
            System.out.println("Wybrano zakładkę \"Szerokość\"");
            WebElement chooseWidth = getDriver().findElement(By.xpath("//*[contains(@text, '" + width + "')]"));
            chooseWidth.click();
            System.out.println("Wybrano szerokość: " + width);
            WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
            ready.click();
        } else {
        }
    }

    @Step("Setting inches")
    public void searchInches(boolean cale, String inch) {
        while (helper.swipeToElementByText("Cale")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        if (cale) {
//            helper.tapCoordinates(100, 1084);
            WebElement inches = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Cale" + "')]"));
            inches.click();
            System.out.println("Wybrano zakładkę \"Cale\"");
            WebElement chooseInches = getDriver().findElement(By.xpath("//*[contains(@text, '" + inch + "')]"));
            chooseInches.click();
            System.out.println("Wybrano ilość cali: " + inch);
            WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
            ready.click();
        } else {
        }
    }

    @Step("Setting type of tire")
    public void searchTireType(boolean tiretype, String tireType) {
        while (helper.swipeToElementByText("Typ opon")) {
            helper.swipeInDirection(direction.UP, "up", 0.5);
        }
        if (tiretype) {
//            helper.tapCoordinates(142, 1281);
            WebElement tireTypes = getDriver().findElement(By.xpath("//*[contains(@text, '" + "Typ opon" + "')]"));
            tireTypes.click();
            System.out.println("Wybrano zakładkę \"Typ opon\"");
            WebElement chooseTireType = getDriver().findElement(By.xpath("//*[contains(@text, '" + tireType + "')]"));
            chooseTireType.click();
            System.out.println("Wybrano typ opon: " + tireType);
            WebElement ready = getDriver().findElement(By.xpath("//*[contains(@text, '" + "GOTOWE" + "')]"));
            ready.click();
        } else {
        }
    }


    //endregion METHODS

}
