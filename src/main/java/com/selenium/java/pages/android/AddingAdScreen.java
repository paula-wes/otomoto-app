package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

public class AddingAdScreen extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodaj ogłoszenie')]")
    public WebElement btn_addStart;
    @FindBy(how = How.ID, using = "pl.otomoto:id/imageView")
    public WebElement btn_addPhoto;
    @FindBy(how = How.CLASS_NAME, using = "android.widget.ImageView")
    public List<WebElement> list_image;
    @FindBy(how = How.ID, using = "pl.otomoto:id/action_done")
    public WebElement btn_confirmPhotos;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Motocykle')]")
    public WebElement btn_motorcycle;
    @FindBy(how = How.ID, using = "pl.otomoto:id/spinner")
    public WebElement btn_brand;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Model')]")
    public WebElement btn_model;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Rok')]")
    public WebElement input_year;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Przebieg')]")
    public WebElement input_mileage;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'VIN')]")
    public WebElement input_vin;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Benzyna')]")
    public WebElement btn_gasoline;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Diesel')]")
    public WebElement btn_diesel;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Elektryczny')]")
    public WebElement btn_fElectric;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Cena')]")
    public WebElement input_price;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Do negocjacji')]")
    public WebElement btn_nego;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'VAT marża')]")
    public WebElement btn_vat;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Możliwość finansowania')]")
    public WebElement btn_finance;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Faktura VAT')]")
    public WebElement btn_invoice;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Leasing')]")
    public WebElement btn_leasing;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Opis')]")
    public WebElement input_descr;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Kolor')]")
    public WebElement btn_color;
    @FindBy(how = How.ID, using = "pl.otomoto:id/widget_form_accordion_block_more")
    public WebElement btn_expand;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Metalik')]")
    public WebElement btn_metalic;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Perłowy')]")
    public WebElement btn_pearl;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Matowy')]")
    public WebElement btn_mat;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Typ Silnika')]")
    public WebElement btn_engineType;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dwusuwowy')]")
    public WebElement btn_twoStroke;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Czterosuwowy')]")
    public WebElement btn_fourStroke;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Elektryczny')]")
    public WebElement btn_electric;
    @FindBy(how = How.ID, using = "pl.otomoto:id/previewBtn")
    public WebElement btn_preview;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Lokalizacja')]")
    public WebElement btn_location;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Osoba kontaktowa')]")
    public WebElement input_person;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Numer telefonu')]")
    public WebElement input_phone;
    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wyrażam')]")
    public WebElement btn_checkboxAd;

    @Step("Start adding motorcycle ad")
    public void addAdMotorcycleStart() {
        methodHelper.waitTime(1);
        methodHelper.tapCoordinates(100, 200);
        methodHelper.waitTime(2);
        btn_addStart.click();
        btn_motorcycle.click();
    }

    @Step("Adding photos of motorcycle")
    public void addPhoto(int first, int second, int third, int fourth) {
        btn_addPhoto.click();
        for (int p = 0; p < list_image.size(); p++) {
            if (p == first) {
                list_image.get(first).click();
                list_image.get(second).click();
                list_image.get(third).click();
                list_image.get(fourth).click();
            }
        }
        btn_confirmPhotos.click();
    }

    @Step("Choosing motorcycle brand")
    public void addBrand(String brand) {
        btn_brand.click();
        WebElement element = getDriver().findElement(By.xpath("//*[contains(@text, 'Access')]"));
        int leftX = element.getLocation().getX();
        int rightX = leftX + element.getSize().getWidth();
        int middleX = (rightX + leftX) / 2;  // tutaj środek elemntu poziomo
        int upperY = element.getLocation().getY();
        int lowerY = upperY + element.getSize().getHeight();
        int middleY = (upperY + lowerY) / 2; // środek elementu pionowo

        methodHelper.swipeToElementByTextAndCordinates(brand, middleX, 2577, middleX, 1057);
        WebElement chooseBrand = getDriver().findElement(By.xpath("//*[contains(@text, '" + brand + "')]"));
        chooseBrand.click();

    }

    @Step("Choosing motorcycle model")
    public void addModel(String model) {
        btn_model.click();
        WebElement element = getDriver().findElement(By.xpath("//*[contains(@text, 'Inny')]"));
        int leftX = element.getLocation().getX();
        int rightX = leftX + element.getSize().getWidth();
        int middleX = (rightX + leftX) / 2;  // tutaj środek elemntu poziomo
        int upperY = element.getLocation().getY();
        int lowerY = upperY + element.getSize().getHeight();
        int middleY = (upperY + lowerY) / 2; // środek elementu pionowo
        methodHelper.swipeToElementByTextAndCordinates(model, middleX, 2577, middleX, 1057);
        WebElement chooseBrand = getDriver().findElement(By.xpath("//*[contains(@text, '" + model + "')]"));
        chooseBrand.click();
    }

    @Step("Adding motorcycle year of production")
    public void addYearOfProduction(String year) {
        input_year.sendKeys(year);
    }

    @Step("Adding motorcycle mileage")
    public void addMileage(String mileage) {
        input_mileage.sendKeys(mileage);
    }

    @Step("Adding motorcycle VIN")
    public void addVin(String vin) {
        input_vin.sendKeys(vin);
        methodHelper.waitTime(2);
        methodHelper.scrollText("Do negocjacji", direction.UP, "up", 0.6, 1);
    }

    @Step("Choosing fuel")
    public void addFuel(boolean gasoline, boolean diesel, boolean fElectric) {

        if (gasoline) {
            for (int i = 0; i < 2; i++) {
                btn_gasoline.click();
            }
        }
        if (diesel) {
            btn_diesel.click();
        }
        if (fElectric) {
            btn_fElectric.click();
        }
    }

    @Step("Adding price")
    public void addPrice(String price) {
        input_price.sendKeys(price);

    }

    @Step("Setting additional price attributes")
    public void addPriceAttributes(boolean nego, boolean vat, boolean finance, boolean invoice, boolean leasing) {

        btn_expand.click();
        methodHelper.scrollText("Opis", direction.UP, "up", 0.4, 1);

        if (nego) {
            btn_nego.click();
        }

        if (vat) {
            btn_vat.click();
        }

        if (finance) {
            btn_finance.click();
        }

        if (invoice) {
            btn_invoice.click();
        }

        if (leasing) {
            btn_leasing.click();
        }

    }

    @Step("Adding description")
    public void addDescr(String descr) {

        input_descr.sendKeys(descr);

    }
    @Step("Scrolling to the nex step")
    public void addScrollAndExpand() {
        methodHelper.scrollText("INFORMACJE", direction.UP, "up", 0.6, 1);
        btn_expand.click();
    }

    @Step("Choosing motorcycle type")
    public void addType(String type) {


        WebElement element = getDriver().findElement(By.xpath("//*[contains(@text, 'Chopper')]"));
        int leftX = element.getLocation().getX();
        int rightX = leftX + element.getSize().getWidth();
        int middleX = (rightX + leftX) / 2;  // tutaj środek elemntu poziomo
        int upperY = element.getLocation().getY();
        int lowerY = upperY + element.getSize().getHeight();
        int middleY = (upperY + lowerY) / 2; // środek elementu pionowo

        methodHelper.swipeToElementByTextAndCordinates(type, 1140, middleY, 240, middleY);
        WebElement chooseType = getDriver().findElement(By.xpath("//*[contains(@text, '" + type + "')]"));
        chooseType.click();
        methodHelper.waitTime(2);
    }

    @Step("Setting engine type")
    public void addEngineType(boolean twoStroke, boolean fourStroke, boolean electric) {
        btn_engineType.click();

        if (twoStroke) {
            btn_twoStroke.click();
        }
        if (fourStroke) {
            btn_fourStroke.click();
        }
        if (electric) {
            btn_electric.click();
        }
    }

    @Step("Setting colour parameters")
    public void addColourParameters(boolean metalic, boolean pearl, boolean mat) {
        if (metalic) {
            btn_metalic.click();
        }

        if (pearl) {
            btn_pearl.click();
        }

        if (mat) {
            btn_mat.click();
        }
    }

    @Step("Setting colour")
    public void addColour(String colour) {
        btn_color.click();
        methodHelper.scrollText(colour, direction.UP, "up", 0.6, 1);
        WebElement chooseColor = getDriver().findElement(By.xpath("//*[contains(@text, '" + colour + "')]"));
        chooseColor.click();


    }

    @Step("Setting contact person")
    public void addContactPerson(String person) {
        methodHelper.scrollText("Wyrażam", direction.UP, "up", 0.8, 1);
        input_person.sendKeys(person);

    }

    @Step("Setting phone number")
    public void addPhoneNumber(String number) {

        input_phone.sendKeys(number);

    }

    @Step("Confirming approvals")
    public void addCheckboxes() {

        btn_checkboxAd.click();
    }

    @Step("Setting location")
    public void addLocation(String voivodeship, String city) {

        btn_location.click();


        methodHelper.scrollText(voivodeship, direction.UP, "up", 0.3, 1);

        WebElement chooseVoivodeship = getDriver().findElement(By.xpath("//*[contains(@text, '" +
                voivodeship + "')]"));
        chooseVoivodeship.click();

        methodHelper.waitTime(2);

        methodHelper.scrollText(city, direction.UP, "up", 0.3, 1);
        WebElement chooseCity = getDriver().findElement(By.xpath("//*[contains(@text, '" + city + "')]"));
        chooseCity.click();
    }

    @Step("Ending motorcycle ad adding")
    public void addAdMotorcycleEnd() {
        btn_preview.click();
    }

    @Step("Checking assertion for incorrect adding")
    public void addAssertIncorrect() {
        try {
            getDriver().findElement(By.id("pl.otomoto:id/previewBtn"));
        } catch (java.util.NoSuchElementException e) {
            Assert.fail("Error on Page: element not found");
        }
        Assert.assertTrue(getDriver().findElement
                (By.id("pl.otomoto:id/previewBtn")).isDisplayed());
    }
}
