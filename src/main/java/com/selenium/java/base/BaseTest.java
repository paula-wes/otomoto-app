package com.selenium.java.base;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.MalformedParameterizedTypeException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BaseTest {


    public static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();
    private static ThreadLocal<WebDriverWait> wait = new ThreadLocal<>();
    public boolean instalApp = true;
    public boolean headless = false;
    public String url;
    private static ChromeDriverService service;

    private static AppiumDriverLocalService server;

    public static void startChromeServer() {
        service = new ChromeDriverService.Builder()
                .usingAnyFreePort()
                .build();
        try {
            service.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void startAppiumServer() {
        AppiumServiceBuilder serviceBuilder = new AppiumServiceBuilder();
        serviceBuilder.withArgument(GeneralServerFlag.LOG_LEVEL, "warn");
        serviceBuilder.withIPAddress("127.0.0.1");
        serviceBuilder.usingAnyFreePort();

        server = AppiumDriverLocalService.buildService(serviceBuilder);
        server.start();
    }

    private static String platformName;
    private static ThreadLocal<String> webXmlTestName = new ThreadLocal<>();
    private static ThreadLocal<String> className = new ThreadLocal<>();

    protected void setupAndroidRemoteDriver(String platform, String deviceId, String deviceName, String xmlTestName) throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("className", this.getClass().getSimpleName());
        capabilities.setCapability("deviceName", deviceName);
        capabilities.setCapability("currentXmlTest", xmlTestName);
        capabilities.setCapability(MobileCapabilityType.UDID, deviceId);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, platform);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);

        if (instalApp) {
            String pathToFile = "";
            capabilities.setCapability(MobileCapabilityType.APP, pathToFile);
        } else {
            capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "pl.otomoto");
        }

        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,
                "com.fixeads.verticals.cars.startup.view.activities.SplashActivity");
        capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
        capabilities.setCapability("uiautomator2ServerInstallTimeout", 90000);
        capabilities.setCapability("uiautomator2ServerLaunchTimeout", 90000);
        capabilities.setCapability(AndroidMobileCapabilityType.ANDROID_INSTALL_TIMEOUT, 1000);
        capabilities.setCapability(AndroidMobileCapabilityType.DEVICE_READY_TIMEOUT, 10000);
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60000);

        driver.set(new AndroidDriver<AndroidElement>(new URL(server.getUrl().toString()), capabilities));
    }

    public void launchAndroid(String platform, String deviceId, String deviceName, ITestContext context) {

        platformName = platform;

        try {
            setupAndroidRemoteDriver(platform, deviceId, deviceName, context.getCurrentXmlTest().getName());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        System.out.println("Session ID: " + driver.get().getSessionId());
        System.out.println("Server address: 127.0.0.1");
        System.out.println("Appium Port: " + server.getUrl().getPort());

        getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    public void launchWeb(String platform, String browser, ITestContext context) {
        platformName = platform;
        if (browser.equals("Chrome")) {
            setupChromeWebRemoteDriver(platform, headless, context.getCurrentXmlTest().getName());
        } else if (browser.equals("Firefox")) {
            setupFirefoxWebRemoteDriver(platform, headless, context.getCurrentXmlTest().getName());
        } else {
            System.out.println("Incorrect Browser");
        }
        getDriver().manage().window().maximize();
        getDriver().manage().timeouts().pageLoadTimeout(55, TimeUnit.SECONDS);
        getDriver().manage().timeouts().implicitlyWait(55, TimeUnit.SECONDS);
        getDriver().navigate().to(url);
    }

    protected void setupChromeWebRemoteDriver(String platform, Boolean headless, String xmlTestName) {
        startChromeServer();
        platformName = platform;
        webXmlTestName.set(xmlTestName);
        className.set(this.getClass().getSimpleName());
        ChromeOptions chromeOptions = new ChromeOptions();
        if (headless) {
            chromeOptions.addArguments("--headless");
        }
        chromeOptions.addArguments("--window-size=2024,1024");
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("intl.accept_languages", "EN");
        chromeOptions.setExperimentalOption("prefs", prefs);
//        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        chromeOptions.addArguments("disable-extensions");
        chromeOptions.addArguments("ignore-certificate-errors");
        chromeOptions.addArguments("no-default-browser-check");
        chromeOptions.addArguments("test-type");
        driver.set(new RemoteWebDriver(service.getUrl(), chromeOptions));
    }


    protected void setupFirefoxWebRemoteDriver(String platform, Boolean headless, String xmlTestName) {
        platformName = platform;
        webXmlTestName.set(xmlTestName);
        className.set(this.getClass().getSimpleName());
        FirefoxBinary firefoxBinary = new FirefoxBinary();
        if (headless) {
            firefoxBinary.addCommandLineOptions("--headless");
        }
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("intl.accept_languages","en");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setProfile(firefoxProfile);
        firefoxOptions.setBinary(firefoxBinary);
        firefoxOptions.addArguments("--width=2024");
        firefoxOptions.addArguments("--height=1024");
        firefoxOptions.addPreference("geo.enabled", false);
        firefoxOptions.addPreference("geo.provider.use_corelocation", false);
        firefoxOptions.addPreference("geo.prompt.testing", false);
        firefoxOptions.addPreference("geo.prompt.testing.allow", false);
        firefoxOptions.addPreference("--headless", true);
//        firefoxOptions.addPreference("--lang", "PL");
        driver.set(new FirefoxDriver(firefoxOptions));
    }

    public enum direction {
        DOWN,
        UP,
        LEFT,
        RIGHT
    }
    private int screenHeight;
    private int screenWidth;
    public int[] getResolutionHandler() {
        WebDriver driver = getDriver();
        Dimension size;
        size = driver.manage().window().getSize();
        screenHeight = (size.height);
        screenWidth = (size.width);
        return new int[]{screenHeight, screenWidth};
    }
    /*
        default value of power is 0.8, should not be less than 0.2
    */
    public void swipeInDirection(direction direction, String place, double power) {
        WebDriver driver = BaseTest.getDriver();
        int res[] = getResolutionHandler();
        int screenHeight = (int) Array.get(res, 0);
        int screenWidth = (int) Array.get(res, 1);
        int startY, startX, endX, endY;
        double multiplierX;
        double multiplierY;
        switch (place) {
            case "left":
                multiplierX = 0.2;
                multiplierY = 0.5;
                break;
            case "right":
                multiplierX = 0.8;
                multiplierY = 0.5;
                break;
            case "up":
                multiplierX = 0.5;
                multiplierY = 0.2;
                break;
            case "down":
                multiplierX = 0.5;
                multiplierY = 0.8;
                break;
            default:
                multiplierX = 0.5;
                multiplierY = 0.5;
                break;
        }
        switch (direction) {
            case DOWN:
                startX = (int) (screenWidth * multiplierX);
                startY = (int) (screenHeight * 0.2);
                endX = (int) (screenWidth * multiplierX);
                endY = (int) (screenHeight * power);
                break;
            case UP:
                startX = (int) (screenWidth * multiplierX);
                startY = (int) (screenHeight * power);
                endX = (int) (screenWidth * multiplierX);
                endY = (int) (screenHeight * 0.2);
                break;
            case RIGHT:
                startX = (int) (screenWidth * power);
                startY = (int) (screenHeight * multiplierY);
                endX = (int) (screenWidth * 0.2);
                endY = (int) (screenHeight * multiplierY);
                break;
            case LEFT:
                startX = (int) (screenWidth * 0.2);
                startY = (int) (screenHeight * multiplierY);
                endX = (int) (screenWidth * power);
                endY = (int) (screenHeight * multiplierY);
                break;
            default:
                throw new IllegalArgumentException("Incorrect direction: " + direction);
        }
        new TouchAction((PerformsTouchActions) driver).press(PointOption.point(startX, startY))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(endX, endY)).release().perform();
    }

    protected static WebDriver getDriver(){
        return driver.get();
    }

    protected static WebDriverWait getWaitDriver(int time) {
        WebDriver currentDriver = getDriver();
        wait.set(new WebDriverWait(currentDriver, time));
        return wait.get();
    }



}
