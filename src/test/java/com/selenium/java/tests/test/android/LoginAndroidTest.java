package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;

import com.selenium.java.listeners.TestListener;

import com.selenium.java.pages.android.LoginScreen;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;


@Listeners({TestListener.class})
@Feature("LOGIN: Login Mobile Tests")
public class LoginAndroidTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
    }


    // region DATAPROVIDERS
    @DataProvider
    public Object[][] wrongPassword() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "Tester2020"},
                {"paula_testuje1@onet.pl", "ster2020OSW"},
        };
    }
    @DataProvider
    public Object[][] wrongEmail() {
        return new Object[][]{
                {"paulatestuje@onet", "Tester2020OSW"},
                {"paula_testuje1@.pl", "Tester2020OSW"},
        };
    }
    @DataProvider
    public Object[][] correctData() {
        return new Object[][]{
                {"paula_testuje@onet.pl", "Tester2020OSW"},
                {"paula_testuje1@onet.pl", "Tester2020OSW"},
        };
    }
    @DataProvider
    public Object[][] noPassword() {
        return new Object[][]{
                {"paula_testuje@onet.pl", ""},
                {"paula_testuje1@onet.pl", ""},
        };
    }
    @DataProvider
    public Object[][] noEmail() {
        return new Object[][]{
                {"", "Tester2020OSW"},
                {"", "Tester2020OSW"},
        };
    }
    //endregion DATAPROVIDERS

    //region TESTS
    MethodHelper helper = new MethodHelper();

    @Test(dataProvider = "wrongPassword",description = "Checking login with wrong password")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login on mobile")
    @Story("LOGIN: log in with wrong password")
    public void testWrongPassLoginToApp(String email, String password) {
        System.out.println("Rozpoczęcie testu logowania się do aplikacji z wpisaniem błędnego hasła");

        WebDriver driver = getDriver();
        LoginScreen logInScreen = PageFactory.initElements(driver, LoginScreen.class);

        logInScreen.loginToApp(email, password);

        helper.testScreenShoot("LoginAndroidTestWrongPass");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/login_layout_btn_login_text")).isDisplayed());
        System.out.println("Test zaliczony - nie zalogowano użytkownika");
    }

    @Test(dataProvider = "wrongEmail",description = "Checking login with wrong e-mail address")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login on mobile")
    @Story("LOGIN: log in with wrong e-mail")

    public void testWrongEmailLoginToApp(String email, String password) {
        System.out.println("Rozpoczęcie testu logowania się do aplikacji z wpisaniem błędnego adresu mailowego");

        WebDriver driver = getDriver();
        LoginScreen logInScreen = PageFactory.initElements(driver, LoginScreen.class);

        logInScreen.loginToApp(email, password);

    }

    @Test(dataProvider = "correctData",description = "Checking login with correct data")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login on mobile")
    @Story("Correct data")
    public void testCorrectDataLoginToApp(String email, String password) {
        System.out.println("Rozpoczęcie testu logowania się do aplikacji z wpisaniem poprawnych danych");

        WebDriver driver = getDriver();
        LoginScreen logInScreen = PageFactory.initElements(driver, LoginScreen.class);

        logInScreen.loginToApp(email, password);
    }

    @Test(dataProvider = "noPassword",description = "Checking login without password")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login on mobile")
    @Story("Without password")
    public void testNoPasswordLoginToApp(String email, String password) {
        System.out.println("Rozpoczęcie testu logowania się do aplikacji bez wpisywania hasła");

        WebDriver driver = getDriver();
        LoginScreen logInScreen = PageFactory.initElements(driver, LoginScreen.class);

        logInScreen.loginToApp(email, password);
    }

    @Test(dataProvider = "noEmail",description = "Checking login without email address")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - login on mobile")
    @Story("Without email")
    public void testNoEmailLoginToApp(String email, String password) {
        System.out.println("Rozpoczęcie testu logowania się do aplikacji bez wpisywania hasła");

        WebDriver driver = getDriver();
        LoginScreen logInScreen = PageFactory.initElements(driver, LoginScreen.class);

        logInScreen.loginToApp(email, password);
    }

    //endregion TESTS
    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        System.out.println("Koniec testu...");
    }
}
