package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.pages.android.AccountScreen;
import com.selenium.java.pages.android.LoginScreen;
import com.selenium.java.pages.android.RegistrationScreen;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;
import io.qameta.allure.Feature;

@Feature("ACCOUNT: Actions on account Tests")
public class AccountAndroidTest extends BaseTest {

    MethodHelper methodHelper = new MethodHelper();

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
        System.out.println("Start of the tests");
    }

    @DataProvider
    public Object[][] getObserve() {
        return new Object[][]{
                {"marasu222@gmail.com", "1234Test1234", false, true},
                {"marek.testowy1@spoko.pl", "123Test123", true, false}
        };
    }

    @DataProvider
    public Object[][] getChat() {
        return new Object[][]{
                {"marasu222@gmail.com", "1234Test1234", "czuki6", "Hello"},
                {"marasu222@gmail.com", "1234Test1234", "gintowt62", "Hi!"}
        };
    }

    @DataProvider
    public Object[][] getMyAccount() {
        return new Object[][]{
                {"marasu222@gmail.com", "1234Test1234"},
                {"marek.testowy1@spoko.pl", "123Test123"}
        };
    }

    @Test(dataProvider = "getObserve", description = "Checking actions in observed ads")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - actions on personal account")
    @Story("ACCOUNT: Observed adds")
    public void testAccObserved(String email, String password, boolean remove, boolean gallery) {

        System.out.println("Acc observed test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        try {
            registrationScreen.btn_cancelSmartLock.click();
            loginScreen.loginToApp(email, password);
        } catch (Exception e) {
            loginScreen.loginToApp(email, password);
        }
        methodHelper.waitTime(3);
        accountScreen.accObserved(remove, gallery);
        methodHelper.getScreenShot("Observed.png");
        methodHelper.testScreenshot("Observed");
    }

    @Test(dataProvider = "getChat", description = "Checking actions in chat")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - actions on personal account")
    @Story("ACCOUNT: Chat actions")
    public void testAccChat(String email, String password, String nick, String chat) {

        System.out.println("Acc chat test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        try {
            registrationScreen.btn_cancelSmartLock.click();
            loginScreen.loginToApp(email, password);
        } catch (Exception e) {
            loginScreen.loginToApp(email, password);
        }
        methodHelper.waitTime(3);
        accountScreen.accChat(nick, chat);
        methodHelper.getScreenShot("Chat.png");
        methodHelper.testScreenshot("Chat");

    }

    @Test(dataProvider = "getMyAccount", description = "Checking actions in statistics")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - actions on personal account")
    @Story("ACCOUNT: Statistics")
    public void testAccMyAccountStats(String email, String password) {

        System.out.println("Acc stats test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AccountScreen accountScreen = PageFactory.initElements(driver, AccountScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        try {
            registrationScreen.btn_cancelSmartLock.click();
            loginScreen.loginToApp(email, password);

        } catch (Exception e) {
            loginScreen.loginToApp(email, password);
        }
        methodHelper.waitTime(3);
        accountScreen.accMyAccountStats();
        methodHelper.getScreenShot("Stats.png");
        methodHelper.testScreenshot("Stats");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        System.out.println("End of the tests");
    }
}