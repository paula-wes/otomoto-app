package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.RegistrationPage;
import com.selenium.java.pages.web.SearchCarPage;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners({TestListener.class})
@Feature("WEB REGISTRATION: Registration Web Tests")
public class RegistrationWebTest extends BaseTest{

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = true;
        url = "https://www.otomoto.pl/";

        launchWeb(platform, browser, context);
        System.out.println("Test started");

    }

    @DataProvider
    public Object[][] getReg(){
        return new Object[][]{
                {"damian1@vp.pl", "Haslo123", "Haslo123", true},
                {"damian2@vp.pl", "Haslo123", "Haslo123", true},
                {"damian3@vp.pl", "Haslo123", "Haslo123", true}
        };
    }

    @DataProvider
    public Object[][] getFailEmailReg(){
        return new Object[][]{
                {"damian1vp.pl", "Haslo123", "Haslo123", true},
                {"damian2vp.pl", "Haslo123", "Haslo123", true},
                {"damian3vp.pl", "Haslo123", "Haslo123", true}
        };
    }

    @DataProvider
    public Object[][] getNoEmailReg(){
        return new Object[][]{
                {"", "Haslo123", "Haslo123", true},
                {"", "Haslo1223", "Haslo1223", true},
                {"", "Haslo13", "Haslo13", true}
        };
    }

    @DataProvider
    public Object[][] getFailPassReg(){
        return new Object[][]{
                {"damian1@vp.pl", "Haslo", "Haslo", true},
                {"damian2@vp.pl", "Hasl", "Hasl", true},
                {"damian3@vp.pl", "Has", "Has", true}
        };
    }

    @DataProvider
    public Object[][] getNoPassReg(){
        return new Object[][]{
                {"damian1@vp.pl", "", "Haslo123", true},
                {"damian2@vp.pl", "", "Haslo123", true},
                {"damian3@vp.pl", "", "Haslo123", true}
        };
    }

    @DataProvider
    public Object[][] getFailRepassReg(){
        return new Object[][]{
                {"damian1@vp.pl", "Haslo123", "aslo123", true},
                {"damian2@vp.pl", "Haslo123", "Haslo13", true},
                {"damian3@vp.pl", "Haslo123", "Halo123", true}
        };
    }

    @DataProvider
    public Object[][] getNoRepassReg(){
        return new Object[][]{
                {"damian1@vp.pl", "Haslo123", "", true},
                {"damian2@vp.pl", "Haslo123", "", true},
                {"damian3@vp.pl", "Haslo123", "", true}
        };
    }


    @DataProvider
    public Object[][] getNoPolicyReg(){
        return new Object[][]{
                {"damian@vp.pl", "Haslo123", "Haslo123", false},
                {"damian2@vp.pl", "Haslo123", "Haslo123", false},
                {"damian3@vp.pl", "Haslo123", "Haslo123", false}
        };
    }

    @Test(dataProvider = "getReg", description = "Checking correct registration")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration to application")
    @Story("REGISTRATION: All inputs correct")
    public void regTest(String email, String pass, String repass, boolean policy) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.regMyAccount();
        methodHelper.waitTime(2);
        registrationPage.regMoveToReg();
        methodHelper.waitTime(2);
        registrationPage.regSetEmail(email);
        methodHelper.waitTime(2);
        registrationPage.regSetPassword(pass);
        methodHelper.waitTime(2);
        registrationPage.regSetRepassword(repass);
        methodHelper.waitTime(2);
        registrationPage.regCookies();
        methodHelper.waitTime(2);
        registrationPage.regCheckPolicy(policy);
        methodHelper.waitTime(2);
        registrationPage.regSingIn();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("regTest");
        methodHelper.waitTime(2);
        registrationPage.regAssert();



    }

    @Test(dataProvider = "getFailEmailReg", description = "Checking registration to app with wrong email")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration to application")
    @Story("REGISTRATION: Wrong email")
    public void regFailEmailTest(String email, String pass, String repass, boolean policy) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.regMyAccount();
        methodHelper.waitTime(2);
        registrationPage.regMoveToReg();
        methodHelper.waitTime(2);
        registrationPage.regSetEmail(email);
        methodHelper.waitTime(2);
        registrationPage.regSetPassword(pass);
        methodHelper.waitTime(2);
        registrationPage.regSetRepassword(repass);
        methodHelper.waitTime(2);
        registrationPage.regCookies();
        methodHelper.waitTime(2);
        registrationPage.regCheckPolicy(policy);
        methodHelper.waitTime(2);
        registrationPage.regSingIn();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("regTest");
        methodHelper.waitTime(2);
        registrationPage.regFailAssert();



    }


    @Test(dataProvider = "getNoEmailReg", description = "Checking registration to app without email")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration to application")
    @Story("REGISTRATION: No email")
    public void regNoEmailTest(String email, String pass, String repass, boolean policy) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.regMyAccount();
        methodHelper.waitTime(2);
        registrationPage.regMoveToReg();
        methodHelper.waitTime(2);
        registrationPage.regSetEmail(email);
        methodHelper.waitTime(2);
        registrationPage.regSetPassword(pass);
        methodHelper.waitTime(2);
        registrationPage.regSetRepassword(repass);
        methodHelper.waitTime(2);
        registrationPage.regCookies();
        methodHelper.waitTime(2);
        registrationPage.regCheckPolicy(policy);
        methodHelper.waitTime(2);
        registrationPage.regSingIn();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("regTest");
        methodHelper.waitTime(2);
        registrationPage.regFailAssert();

    }


    @Test(dataProvider = "getFailPassReg", description = "Checking registration to app with too short pass")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration to application")
    @Story("REGISTRATION: Too short password")
    public void regFailPasswordTest(String email, String pass, String repass, boolean policy) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.regMyAccount();
        methodHelper.waitTime(2);
        registrationPage.regMoveToReg();
        methodHelper.waitTime(2);
        registrationPage.regSetEmail(email);
        methodHelper.waitTime(2);
        registrationPage.regSetPassword(pass);
        methodHelper.waitTime(2);
        registrationPage.regSetRepassword(repass);
        methodHelper.waitTime(2);
        registrationPage.regCookies();
        methodHelper.waitTime(2);
        registrationPage.regCheckPolicy(policy);
        methodHelper.waitTime(2);
        registrationPage.regSingIn();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("regTest");
        methodHelper.waitTime(2);
        registrationPage.regFailAssert();

    }


    @Test(dataProvider = "getFailPassReg", description = "Checking registration to app without password")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration to application")
    @Story("REGISTRATION: No password")
    public void regNoPasswordTest(String email, String pass, String repass, boolean policy) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.regMyAccount();
        methodHelper.waitTime(2);
        registrationPage.regMoveToReg();
        methodHelper.waitTime(2);
        registrationPage.regSetEmail(email);
        methodHelper.waitTime(2);
        registrationPage.regSetPassword(pass);
        methodHelper.waitTime(2);
        registrationPage.regSetRepassword(repass);
        methodHelper.waitTime(2);
        registrationPage.regCookies();
        methodHelper.waitTime(2);
        registrationPage.regCheckPolicy(policy);
        methodHelper.waitTime(2);
        registrationPage.regSingIn();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("regTest");
        methodHelper.waitTime(2);
        registrationPage.regFailAssert();

    }

    @Test(dataProvider = "getFailPassReg", description = "Checking registration to app with wrong repass")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration to application")
    @Story("REGISTRATION: Wrong repassword")
    public void regFailRepasswordTest(String email, String pass, String repass, boolean policy) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.regMyAccount();
        methodHelper.waitTime(2);
        registrationPage.regMoveToReg();
        methodHelper.waitTime(2);
        registrationPage.regSetEmail(email);
        methodHelper.waitTime(2);
        registrationPage.regSetPassword(pass);
        methodHelper.waitTime(2);
        registrationPage.regSetRepassword(repass);
        methodHelper.waitTime(2);
        registrationPage.regCookies();
        methodHelper.waitTime(2);
        registrationPage.regCheckPolicy(policy);
        methodHelper.waitTime(2);
        registrationPage.regSingIn();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("regTest");
        methodHelper.waitTime(2);
        registrationPage.regFailAssert();

    }


    @Test(dataProvider = "getFailPassReg", description = "Checking registration to app without repassword")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration to application")
    @Story("REGISTRATION: No repassword")
    public void regNoRepasswordTest(String email, String pass, String repass, boolean policy) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.regMyAccount();
        methodHelper.waitTime(2);
        registrationPage.regMoveToReg();
        methodHelper.waitTime(2);
        registrationPage.regSetEmail(email);
        methodHelper.waitTime(2);
        registrationPage.regSetPassword(pass);
        methodHelper.waitTime(2);
        registrationPage.regSetRepassword(repass);
        methodHelper.waitTime(2);
        registrationPage.regCookies();
        methodHelper.waitTime(2);
        registrationPage.regCheckPolicy(policy);
        methodHelper.waitTime(2);
        registrationPage.regSingIn();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("regTest");
        methodHelper.waitTime(2);
        registrationPage.regFailAssert();


    }


    @Test(dataProvider = "getFailPassReg", description = "Checking registration to app without accept policy")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - registration to application")
    @Story("REGISTRATION: No accept policy")
    public void regNoPolicyTest(String email, String pass, String repass, boolean policy) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.regMyAccount();
        methodHelper.waitTime(2);
        registrationPage.regMoveToReg();
        methodHelper.waitTime(2);
        registrationPage.regSetEmail(email);
        methodHelper.waitTime(2);
        registrationPage.regSetPassword(pass);
        methodHelper.waitTime(2);
        registrationPage.regSetRepassword(repass);
        methodHelper.waitTime(2);
        registrationPage.regCookies();
        methodHelper.waitTime(2);
        registrationPage.regCheckPolicy(policy);
        methodHelper.waitTime(2);
        registrationPage.regSingIn();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("regTest");
        methodHelper.waitTime(2);
        registrationPage.regFailAssert();


    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
        //  driver.close();
    }

}
