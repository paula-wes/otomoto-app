package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.pages.android.SearchPartsScreen;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;


@Feature("SEARCHING: Vehicle's parts search Mobile Tests")
public class SearchPartsAndroidTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
    }

    //region DATAPROVIDERS
    @DataProvider
    public Object[][] searchMotorcycleParts() {
        return new Object[][]{
                {"Tak", "Prywatne", "Wszystkie", "Felgi", "Kawasaki", "5000", "", "Małopolskie"},
        };
    }

    @DataProvider
    public Object[][] searchCarParts() {
        return new Object[][]{
                {"Tak", "Prywatne", "EUR", "Koła", "Toyota", "100", "300", "Małopolskie"},
                {"Nie", "Firma", "PLN", "Układ napędowy", "Nissan", "300", "1000", "Pomorskie"},
        };
    }

    @DataProvider
    public Object[][] searchTruckParts() {
        return new Object[][]{
                {"Tak", "Prywatne", "EUR", "Koła", "Kia", "100", "300", "Małopolskie"},
                {"Nie", "Firma", "PLN", "Układ napędowy", "Scania", "300", "5000", "Pomorskie"},
        };
    }

    @DataProvider
    public Object[][] searchDeliveryVehicleParts() {
        return new Object[][]{
                {"Tak", "Firma", "Wszystkie", "Koła", "Ford", "100", "300", "Małopolskie"},
                {"Nie", "Prywatne", "PLN", "Sprzęt car audio", "Iveco", "100", "1000", "Pomorskie"},
        };
    }

    @DataProvider
    public Object[][] searchVehiclesForParts() {
        return new Object[][]{
                {"Tak", "Firma", "Wszystkie", "1000", "30000", "Małopolskie"},
                {"Nie", "Prywatne", "PLN", "1000", "10000", "Pomorskie"},
        };
    }

    @DataProvider
    public Object[][] searchAgriculturalVehiclesParts() {
        return new Object[][]{
                {"Tak", "Firma", "Wszystkie", "Scania", "Koła", "10", "3000", true, "Wszystkie", true, "Aluminiowe", true, "Wszystkie", true, "22 i większe", true, "Zimowe", "Małopolskie"},
        };
    }

    @DataProvider
    public Object[][] searchConstructionVehiclesParts() {
        return new Object[][]{
                {"Tak", "Firma", "Wszystkie", "Scania", "Koła", "10", "3000", true, "Wszystkie", true, "Stalowe", true, "Wszystkie", true, "22 i większe", true, "Zimowe", "Małopolskie"},
                {"Nie", "Prywatne", "PLN", "Iveco", "Sprzęt car audio", "100", "10000", false, "", false, "", false, "", false, "", false, "", "Wielkopolskie"},
        };
    }

    @DataProvider
    public Object[][] searchOtherParts() {
        return new Object[][]{
                {"Tak", "Firma", "Wszystkie", "Koła", "10", "3000", "Małopolskie"},
                {"Nie", "Prywatne", "PLN", "Sprzęt car audio", "100", "10000", "Wielkopolskie"},
        };
    }

    @DataProvider
    public Object[][] searchAllInParts() {
        return new Object[][]{
                {"Tak", "Firma", "Wszystkie", "Koła", "10", "3000", "Małopolskie"},
                {"Nie", "Prywatne", "PLN", "Sprzęt car audio", "100", "10000", "Wielkopolskie"},
        };
    }

    //endregion DATAPROVIDERS

    //region TESTS
    MethodHelper helper = new MethodHelper();

    //region TEST - motorcycle parts searching
    @Test(dataProvider = "searchMotorcycleParts", description = "Searching item in Motorcycle Parts category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search motorcycle parts")
    public void testMotorcyclePartsSearching(String deliveryOption, String privacy, String currency, String partName,
                                             String vehicleBrand, String priceMin, String priceMax, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Części\"");

        WebDriver driver = getDriver();
        SearchPartsScreen spartsScreen = PageFactory.initElements(driver, SearchPartsScreen.class);

        spartsScreen.searchSetParts("CZĘŚCI");
        spartsScreen.searchApplication("Części motocyklowe");
        spartsScreen.searchDelivery(deliveryOption);
        spartsScreen.searchMoreParameters();
        spartsScreen.searchSetPrivacy(privacy);
        spartsScreen.searchSetCurrency(currency);
        spartsScreen.searchTypeOfParts(partName);
        spartsScreen.searchVehicleBrand(vehicleBrand);
        spartsScreen.searchSetPrice(priceMin, priceMax);
        spartsScreen.searchLocation(voivodeship);
        spartsScreen.searchShowResults();

        helper.testScreenShoot("SearchPartsAndroidTestMotorcycleParts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - motorcycle parts searching

    //region TEST - car parts searching
    @Test(dataProvider = "searchCarParts", description = "Searching item in Car Parts category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search car parts")
    public void testCarPartsSearching(String deliveryOption, String privacy, String currency, String partName,
                                      String vehicleBrand, String priceMin, String priceMax, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Części\"");

        WebDriver driver = getDriver();
        SearchPartsScreen spartsScreen = PageFactory.initElements(driver, SearchPartsScreen.class);

        spartsScreen.searchSetParts("CZĘŚCI");
        spartsScreen.searchApplication("Części samochodowe");
        spartsScreen.searchDelivery(deliveryOption);
        spartsScreen.searchMoreParameters();
        spartsScreen.searchSetPrivacy(privacy);
        spartsScreen.searchSetCurrency(currency);
        spartsScreen.searchTypeOfParts(partName);
        spartsScreen.searchVehicleBrand(vehicleBrand);
        spartsScreen.searchSetPrice(priceMin, priceMax);
        spartsScreen.searchLocation(voivodeship);
        spartsScreen.searchShowResults();

        helper.testScreenShoot("SearchPartsAndroidTestCarParts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - car parts searching

    //region TEST - truck parts searching
    @Test(dataProvider = "searchTruckParts", description = "Searching item in Truck Parts category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search truck parts")
    public void testTruckPartsSearching(String deliveryOption, String privacy, String currency, String partName,
                                        String vehicleBrand, String priceMin, String priceMax, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Części\"");

        WebDriver driver = getDriver();
        SearchPartsScreen spartsScreen = PageFactory.initElements(driver, SearchPartsScreen.class);

        spartsScreen.searchSetParts("CZĘŚCI");
        spartsScreen.searchApplication("Części do pojazdów ciężarowych");
        spartsScreen.searchDelivery(deliveryOption);
        spartsScreen.searchMoreParameters();
        spartsScreen.searchSetPrivacy(privacy);
        spartsScreen.searchSetCurrency(currency);
        spartsScreen.searchTypeOfParts(partName);
        spartsScreen.searchVehicleBrand(vehicleBrand);
        spartsScreen.searchSetPrice(priceMin, priceMax);
        spartsScreen.searchLocation(voivodeship);
        spartsScreen.searchShowResults();

        helper.testScreenShoot("SearchPartsAndroidTestTruckParts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - truck parts searching

    //region TEST - delivery vehicle parts searching
    @Test(dataProvider = "searchDeliveryVehicleParts", description = "Searching item in Delivery Vehicle Parts category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search delivery vehicle parts")
    public void testDeliveryVehiclePartsSearching(String deliveryOption, String privacy, String currency, String partName,
                                                  String vehicleBrand, String priceMin, String priceMax, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Części\"");

        WebDriver driver = getDriver();
        SearchPartsScreen spartsScreen = PageFactory.initElements(driver, SearchPartsScreen.class);

        spartsScreen.searchSetParts("CZĘŚCI");
        spartsScreen.searchApplication("Części do pojazdów dostawczych");
        spartsScreen.searchDelivery(deliveryOption);
        spartsScreen.searchMoreParameters();
        spartsScreen.searchSetPrivacy(privacy);
        spartsScreen.searchSetCurrency(currency);
        spartsScreen.searchTypeOfParts(partName);
        spartsScreen.searchVehicleBrand(vehicleBrand);
        spartsScreen.searchSetPrice(priceMin, priceMax);
        spartsScreen.searchLocation(voivodeship);
        spartsScreen.searchShowResults();

        helper.testScreenShoot("SearchPartsAndroidTestDeliveryVehicleParts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - delivery vehicle parts searching

    //region TEST - vehicles for parts searching
    @Test(dataProvider = "searchVehiclesForParts", description = "Searching item in Vehicles for Parts category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search vehicles for parts")
    public void testVehiclesForPartsSearching(String deliveryOption, String privacy, String currency, String priceMin,
                                              String priceMax, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Części\"");

        WebDriver driver = getDriver();
        SearchPartsScreen spartsScreen = PageFactory.initElements(driver, SearchPartsScreen.class);

        spartsScreen.searchSetParts("CZĘŚCI");
        spartsScreen.searchApplication("Pojazdy na części");
        spartsScreen.searchDelivery(deliveryOption);
        spartsScreen.searchMoreParameters();
        spartsScreen.searchSetPrivacy(privacy);
        spartsScreen.searchSetCurrency(currency);
        spartsScreen.searchSetPrice(priceMin, priceMax);
        spartsScreen.searchLocation(voivodeship);
        spartsScreen.searchShowResults();

        helper.testScreenShoot("SearchPartsAndroidTestVehiclesForParts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - vehicles for parts searching

    //region TEST - parts for agricultural vehicles and machines searching
    @Test(dataProvider = "searchAgriculturalVehiclesParts", description = "Searching item in Parts for agricultural vehicles " +
            "and machines category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search parts for agricultural vehicles and machines")
    public void testAgriculturalVehiclesPartsSearching(String deliveryOption, String privacy, String currency, String vehicleBrand, String partName,
                                                       String priceMin, String priceMax, boolean profil, String profile, boolean rimtype, String rimType,
                                                       boolean szer, String width, boolean cale, String inch, boolean tiretype, String tireType, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Części\"");

        WebDriver driver = getDriver();
        SearchPartsScreen spartsScreen = PageFactory.initElements(driver, SearchPartsScreen.class);
        spartsScreen.searchSetParts("CZĘŚCI");
        spartsScreen.searchApplication("Części do pojazdów i maszyn rolniczych");
        spartsScreen.searchDelivery(deliveryOption);
        spartsScreen.searchMoreParameters();
        spartsScreen.searchSetPrivacy(privacy);
        spartsScreen.searchSetCurrency(currency);
        spartsScreen.searchVehicleBrand(vehicleBrand);
        spartsScreen.searchTypeOfParts(partName);
        spartsScreen.searchSetPrice(priceMin, priceMax);
        spartsScreen.searchProfile(profil, profile);
        spartsScreen.searchRimType(rimtype, rimType);
        spartsScreen.searchWidth(szer, width);
        spartsScreen.searchInches(cale, inch);
        spartsScreen.searchTireType(tiretype, tireType);
        spartsScreen.searchLocation(voivodeship);
        spartsScreen.searchShowResults();

        helper.testScreenShoot("SearchPartsAndroidTestAgrVehiclesParts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - parts for agricultural vehicles and machines searching

    //region TEST - parts for construction vehicles and machinery searching
    @Test(dataProvider = "searchConstructionVehiclesParts", description = "Searching item in Parts for construction vehicles " +
            "and machinery category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search parts for construction vehicles and machines")
    public void testConstructionsVehiclesPartsSearching(String deliveryOption, String privacy, String currency, String vehicleBrand, String partName,
                                                        String priceMin, String priceMax, boolean profil, String profile, boolean rimtype, String rimType,
                                                        boolean szer, String width, boolean cale, String inch, boolean tiretype, String tireType, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Części\"");

        WebDriver driver = getDriver();
        SearchPartsScreen spartsScreen = PageFactory.initElements(driver, SearchPartsScreen.class);
        spartsScreen.searchSetParts("CZĘŚCI");
        spartsScreen.searchApplication("Części do pojazdów i maszyn budowlanych");
        spartsScreen.searchDelivery(deliveryOption);
        spartsScreen.searchMoreParameters();
        spartsScreen.searchSetPrivacy(privacy);
        spartsScreen.searchSetCurrency(currency);
        spartsScreen.searchVehicleBrand(vehicleBrand);
        spartsScreen.searchTypeOfParts(partName);
        spartsScreen.searchSetPrice(priceMin, priceMax);
        spartsScreen.searchProfile(profil, profile);
        spartsScreen.searchRimType(rimtype, rimType);
        spartsScreen.searchWidth(szer, width);
        spartsScreen.searchInches(cale, inch);
        spartsScreen.searchTireType(tiretype, tireType);
        spartsScreen.searchLocation(voivodeship);
        spartsScreen.searchShowResults();

        helper.testScreenShoot("SearchPartsAndroidTestConstructionsVehicleseParts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - parts for construction vehicles and machinery searching

    //region TEST - other parts searching
    @Test(dataProvider = "searchOtherParts", description = "Searching item in other parts category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search other parts")
    public void testOtherPartsSearching(String deliveryOption, String privacy, String currency, String priceMin,
                                        String priceMax, String partName, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Części\"");

        WebDriver driver = getDriver();
        SearchPartsScreen spartsScreen = PageFactory.initElements(driver, SearchPartsScreen.class);

        spartsScreen.searchSetParts("CZĘŚCI");
        spartsScreen.searchApplication("Pozostałe części");
        spartsScreen.searchDelivery(deliveryOption);
        spartsScreen.searchMoreParameters();
        spartsScreen.searchSetPrivacy(privacy);
        spartsScreen.searchSetCurrency(currency);
        spartsScreen.searchSetPrice(priceMin, priceMax);
        spartsScreen.searchTypeOfParts(partName);
        spartsScreen.searchLocation(voivodeship);
        spartsScreen.searchShowResults();

        helper.testScreenShoot("SearchPartsAndroidTestOtherParts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - other parts searching

    //region TEST - all in parts searching
    @Test(dataProvider = "searchAllInParts", description = "Searching item in all in parts category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search all in parts")
    public void testAllInPartsSearching(String deliveryOption, String privacy, String currency, String priceMin,
                                        String priceMax, String partName, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Części\"");

        WebDriver driver = getDriver();
        SearchPartsScreen spartsScreen = PageFactory.initElements(driver, SearchPartsScreen.class);

        spartsScreen.searchSetParts("CZĘŚCI");
        spartsScreen.searchApplication("Pozostałe części");
        spartsScreen.searchDelivery(deliveryOption);
        spartsScreen.searchMoreParameters();
        spartsScreen.searchSetPrivacy(privacy);
        spartsScreen.searchSetCurrency(currency);
        spartsScreen.searchSetPrice(priceMin, priceMax);
        spartsScreen.searchTypeOfParts(partName);
        spartsScreen.searchLocation(voivodeship);
        spartsScreen.searchShowResults();

        helper.testScreenShoot("SearchPartsAndroidTestAllInParts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion TEST - all in parts searching

    //endregion TESTS


    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        System.out.println("Koniec testu...");
    }


}
