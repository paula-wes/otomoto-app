package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.AddCarPage;
import com.selenium.java.pages.web.LoginPage;
import com.selenium.java.pages.web.RegistrationPage;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.awt.*;

@Listeners({TestListener.class})
@Feature("WEB ADDING: Adding car Web Tests")
public class AddCarWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.otomoto.pl/";

        launchWeb(platform, browser, context);
        System.out.println("Test started");

    }

    @DataProvider
    public Object[][] getAd() {
        return new Object[][]{
                {true, true, true, "xxxxxxxxxxxxxxxxx", "2000", "Aston", "Bulldog", "Benzyna", "120", "1600", "5",
                        "Manualna", "", "", "200000", "Aston Martin Bulldog 2000 rocznik benzyna 1.6", "Kompakt",
                        "Czarny", "10000", "PLN", true, true, "Bydgoszcz"},
                {true, true, true, "xxxxxxxxxxxxxxxxx", "2000", "BMW", "i8", "Benzyna", "120", "1600", "5",
                        "Manualna", "", "", "200000", "BMW 2000 rocznik benzyna 1.6", "Sedan",
                        "Biały", "100000", "EUR", true, true, "Bydgoszcz"},
                {true, true, true, "xxxxxxxxxxxxxxxxx", "2000", "Mazda", "6", "Diesel", "130", "2000", "5",
                        "Automatyczna", "", "I", "200000", "Mazda 6 2000 rocznik benzyna 1.6", "Auta miejskie",
                        "Czarny", "10000", "PLN", true, true, "Bydgoszcz"}
        };
    }


    @Test(dataProvider = "getAd", description = "Checking adding ad")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: All inputs correct")
    public void adTest(boolean imported, boolean damaged, boolean wheel, String vin, String year, String brand,
                       String model, String fuel, String power, String capacity, String doors, String transmission,
                       String version, String generation, String mileage, String title, String type, String colour,
                       String price, String currency, boolean netto, boolean neg, String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);
        methodHelper.waitTime(3);
        addCarPage.addSetYear(year);
        methodHelper.waitTime(3);
        addCarPage.addSetBrand(brand);
        methodHelper.waitTime(3);
        addCarPage.addSetModel(model);
        methodHelper.waitTime(3);
        addCarPage.addSetFuel(fuel);
        methodHelper.waitTime(3);
        addCarPage.addSetEnginePower(power);
        methodHelper.waitTime(3);
        addCarPage.addSetEngineCapacity(capacity);
        methodHelper.waitTime(3);
        addCarPage.addSetDoors(doors);
        methodHelper.waitTime(3);
        addCarPage.addSetTransmission(transmission);
        methodHelper.waitTime(3);
        try {
            addCarPage.addSetGeneration(generation);
            methodHelper.waitTime(3);
            addCarPage.addSetMileage(mileage);
            methodHelper.waitTime(3);
        } catch (Exception e) {
            addCarPage.addSetMileage(mileage);
            methodHelper.waitTime(3);
        }

        addCarPage.addSetTitle(title);
        methodHelper.waitTime(4);
        addCarPage.addSetTypeOfCar(type);
        methodHelper.waitTime(3);
        addCarPage.addSetColour(colour);
        methodHelper.waitTime(3);
        addCarPage.addPhoto();
        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        addCarPage.addSummary();
        methodHelper.waitTime(3);
        methodHelper.testScreenshot("Add car web");

    }

    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - year")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No year of production")
    public void adNoYearTest(boolean imported, boolean damaged, boolean wheel, String vin, String year, String brand,
                       String model, String fuel, String power, String capacity, String doors, String transmission,
                       String version, String generation, String mileage, String title, String type, String colour,
                       String price, String currency, boolean netto, boolean neg, String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);


        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.waitTime(3);
        methodHelper.testScreenshot("Add no year car web");

    }


    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - brand")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No brand")
    public void adNoBrandTest(boolean imported, boolean damaged, boolean wheel, String vin, String year, String brand,
                             String model, String fuel, String power, String capacity, String doors,
                              String transmission, String version, String generation, String mileage, String title,
                              String type, String colour, String price, String currency, boolean netto, boolean neg,
                              String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");

        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);
;
        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");

    }

    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - model")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No model")
    public void adNoModelTest(boolean imported, boolean damaged, boolean wheel, String vin, String year, String brand,
                              String model, String fuel, String power, String capacity, String doors,
                              String transmission, String version, String generation, String mileage, String title,
                              String type, String colour, String price, String currency, boolean netto, boolean neg,
                              String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");

    }

    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - fuel")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No fuel")
    public void adNoFuelTest(boolean imported, boolean damaged, boolean wheel, String vin, String year, String brand,
                              String model, String fuel, String power, String capacity, String doors,
                             String transmission, String version, String generation, String mileage, String title,
                             String type, String colour, String price, String currency, boolean netto, boolean neg,
                             String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetModel(model);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");
    }

    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - power")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No power")
    public void adNoPowerTest(boolean imported, boolean damaged, boolean wheel, String vin, String year, String brand,
                             String model, String fuel, String power, String capacity, String doors,
                              String transmission, String version, String generation, String mileage, String title,
                              String type, String colour, String price, String currency, boolean netto, boolean neg,
                              String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetModel(model);

        addCarPage.addSetFuel(fuel);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");
    }


    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - capacity")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No capacity")
    public void adNoCapacityTest(boolean imported, boolean damaged, boolean wheel, String vin, String year,
                                 String brand, String model, String fuel, String power, String capacity, String doors,
                                 String transmission, String version, String generation, String mileage, String title,
                                 String type, String colour, String price, String currency, boolean netto,
                                 boolean neg, String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetModel(model);

        addCarPage.addSetFuel(fuel);

        addCarPage.addSetEnginePower(power);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");
    }


    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - count of doors")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No doors")
    public void adNoDoorsTest(boolean imported, boolean damaged, boolean wheel, String vin, String year, String brand,
                                 String model, String fuel, String power, String capacity, String doors,
                              String transmission, String version, String generation, String mileage, String title,
                              String type, String colour, String price, String currency, boolean netto,
                              boolean neg, String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetModel(model);

        addCarPage.addSetFuel(fuel);

        addCarPage.addSetEnginePower(power);

        addCarPage.addSetEngineCapacity(capacity);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");
    }


    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - count of doors")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No doors")
    public void adNoTransmissionTest(boolean imported, boolean damaged, boolean wheel, String vin, String year,
                                     String brand, String model, String fuel, String power, String capacity,
                                     String doors, String transmission, String version, String generation,
                                     String mileage, String title, String type, String colour, String price,
                                     String currency, boolean netto, boolean neg, String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetModel(model);

        addCarPage.addSetFuel(fuel);

        addCarPage.addSetEnginePower(power);

        addCarPage.addSetEngineCapacity(capacity);

        addCarPage.addSetDoors(doors);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");
    }

    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - mileage")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No mileage")
    public void adNoMileageTest(boolean imported, boolean damaged, boolean wheel, String vin, String year,
                                     String brand, String model, String fuel, String power, String capacity,
                                     String doors, String transmission, String version, String generation,
                                     String mileage, String title, String type, String colour, String price,
                                     String currency, boolean netto, boolean neg, String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetModel(model);

        addCarPage.addSetFuel(fuel);

        addCarPage.addSetEnginePower(power);

        addCarPage.addSetEngineCapacity(capacity);

        addCarPage.addSetDoors(doors);


        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");
    }

    @Test(dataProvider = "getAd", description = "Checking adding ad without required param - type of car")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No type of car")
    public void adNoTypeTest(boolean imported, boolean damaged, boolean wheel, String vin, String year,
                                String brand, String model, String fuel, String power, String capacity,
                                String doors, String transmission, String version, String generation,
                                String mileage, String title, String type, String colour, String price,
                                String currency, boolean netto, boolean neg, String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetModel(model);

        addCarPage.addSetFuel(fuel);

        addCarPage.addSetEnginePower(power);

        addCarPage.addSetEngineCapacity(capacity);

        addCarPage.addSetDoors(doors);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetColour(colour);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");
    }


    @Test(dataProvider  = "getAd", description = "Checking adding ad without required param - colour")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No colour")
    public void adNoColourTest(boolean imported, boolean damaged, boolean wheel, String vin, String year,
                             String brand, String model, String fuel, String power, String capacity,
                             String doors, String transmission, String version, String generation,
                             String mileage, String title, String type, String colour, String price,
                             String currency, boolean netto, boolean neg, String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();

        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);
        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetModel(model);

        addCarPage.addSetFuel(fuel);

        addCarPage.addSetEnginePower(power);

        addCarPage.addSetEngineCapacity(capacity);

        addCarPage.addSetDoors(doors);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetPrice(price, currency, netto, neg);
        methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");
    }



    @Test(dataProvider  = "getAd", description = "Checking adding ad without required param - price")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding ad in application")
    @Story("ADDING: No price")
    public void adNoPriceTest(boolean imported, boolean damaged, boolean wheel, String vin, String year,
                               String brand, String model, String fuel, String power, String capacity,
                               String doors, String transmission, String version, String generation,
                               String mileage, String title, String type, String colour, String price,
                               String currency, boolean netto, boolean neg, String city) throws AWTException {
        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        AddCarPage addCarPage = PageFactory.initElements(driver, AddCarPage.class);

        registrationPage.regMyAccount();
        loginPage.loginSetEmail("damian.testowy12@vp.pl");
        loginPage.loginSetPassword("Haslo123");
        methodHelper.waitTime(2);
        loginPage.loginSingIn();
        methodHelper.waitTime(2);
        addCarPage.addMoveToAddScreen();
        methodHelper.waitTime(2);
        addCarPage.addCookies();
        methodHelper.waitTime(3);

        addCarPage.addSetBasicInfo(imported, damaged, wheel);
        addCarPage.addSetVin(vin);

        addCarPage.addSetYear(year);

        addCarPage.addSetBrand(brand);

        addCarPage.addSetModel(model);

        addCarPage.addSetFuel(fuel);

        addCarPage.addSetEnginePower(power);

        addCarPage.addSetEngineCapacity(capacity);

        addCarPage.addSetDoors(doors);

        addCarPage.addSetMileage(mileage);

        addCarPage.addSetTitle(title);

        addCarPage.addSetTypeOfCar(type);

        addCarPage.addSetColour(colour);
           methodHelper.waitTime(3);
        Assert.assertTrue(addCarPage.btn_location.isDisplayed());
        methodHelper.testScreenshot("Add no year car web");
    }
    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
        driver.close();
    }

}

