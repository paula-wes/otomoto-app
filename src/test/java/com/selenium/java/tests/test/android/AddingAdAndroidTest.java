package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.pages.android.AddingAdScreen;
import com.selenium.java.pages.android.LoginScreen;
import com.selenium.java.pages.android.RegistrationScreen;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Feature("ADDING AD: Adding motorcycle ad Tests")
public class AddingAdAndroidTest extends BaseTest {
    MethodHelper methodHelper = new MethodHelper();

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
        System.out.println("Start of the tests");
    }

    @DataProvider
    public Object[][] getAttributes() {
        return new Object[][]{
                {"marek.testowy1@spoko.pl", "123Test123", 1, 3, 5, 7, "Aprilia", "AF1", "2002", "40000",
                        "xxxxxxxxxxxxxxxxx", true, false, false, "10000", true, true, true, true, true,
                        "Piękna af1 maly przebieg 1 wlasciciel zapraszam", "Enduro", false, true, false, false, true,
                        false, "Szary", "Jerzy", "758876456", "Kujawsko-pomorskie", "Bydgoszcz"},

//                {"marek.testowy1@spoko.pl", "123Test123", 1, 3, 5, 7, "BMW", "GS", "2012", "12000",
//                        "xxxxxxxxxxxxxxxxx", false, false, true, "90000", true, false, true, false, true,
//                        "Piękny gs maly przebieg 1 wlasciciel zapraszam", "Enduro", true, false, false, false, false,
//                        true, "Czarny", "Arkadiusz", "510482654", "Pomorskie", "Gdańsk"},
//
//                {"marek.testowy1@spoko.pl", "123Test123", 1, 3, 5, 7, "Cagiva", "City", "2008", "27000",
//                        "xxxxxxxxxxxxxxxxx", false, true, false, "15000", false, false, false, false, false,
//                        "Piękna Cagiva maly przebieg 1 wlasciciel zapraszam", "Naked", false, false, true, true, false,
//                        false, "Biały", "Paweł", "602123852", "Łódzkie", "Łódź"}
        };
    }

    @Test(dataProvider = "getAttributes", description = "Adding add with required parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad with all parameters")
    public void testAddTrueMotoAd(String email, String password, int first, int second, int third, int fourth,
                                  String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                  boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                  boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                  boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                  boolean electric, String colour, String person, String number, String voivodeship,
                                  String city) {

        System.out.println("Add motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        System.out.println("Logging to app...");
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        System.out.println("Starting adding motorcycle ad...");
        addingAdScreen.addAdMotorcycleStart();
        System.out.println("Adding photos...");
        addingAdScreen.addPhoto(first, second, third, fourth);
        System.out.println("Choosing brand...");
        addingAdScreen.addBrand(brand);
        System.out.println("Choosing model...");
        addingAdScreen.addModel(model);
        System.out.println("Typing year of production...");
        addingAdScreen.addYearOfProduction(year);
        System.out.println("Typing mileage...");
        addingAdScreen.addMileage(mileage);
        System.out.println("Typing vin...");
        addingAdScreen.addVin(vin);
        System.out.println("Choosing fuel type...");
        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        System.out.println("Typing price...");
        addingAdScreen.addPrice(price);
        System.out.println("Choosing price atributes...");
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        System.out.println("Typing description...");
        addingAdScreen.addDescr(descr);
        System.out.println("Choosing motorcycle type...");
        addingAdScreen.addScrollAndExpand();
        addingAdScreen.addType(type);
        System.out.println("Choosing engine type...");
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        System.out.println("Choosing colour parameters...");
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        System.out.println("Choosing colour...");
        addingAdScreen.addColour(colour);
        System.out.println("Typing contact person...");
        addingAdScreen.addContactPerson(person);
        System.out.println("Typing number...");
        addingAdScreen.addPhoneNumber(number);
        System.out.println("Clicking checkbox...");
        addingAdScreen.addCheckboxes();
        System.out.println("Choosing location...");
        addingAdScreen.addLocation(voivodeship, city);
       // Assert.assertTrue(addingAdScreen.btn_preview.isDisplayed(), "Can't add ad");
        System.out.println("Ending adding ad and clicking preview...");
        addingAdScreen.addAdMotorcycleEnd();
        System.out.println("Test completed, ad added.");
        methodHelper.testScreenshot("Test completed, Ad added");
        methodHelper.getScreenShot("IncorrectBoxesAfterPreview.png");
    }

    @Test(dataProvider = "getAttributes", description = "Adding add without required parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad without brand and model")
    public void testAddMotoAdNoBrand(String email, String password, int first, int second, int third, int fourth,
                                     String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                     boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                     boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                     boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                     boolean electric, String colour, String person, String number, String voivodeship,
                                     String city) {

        System.out.println("Adding motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        addingAdScreen.addAdMotorcycleStart();
        addingAdScreen.addPhoto(first, second, third, fourth);
//         addingAdScreen.addBrand(brand);
//         addingAdScreen.addModel(model);
        addingAdScreen.addYearOfProduction(year);
        addingAdScreen.addMileage(mileage);
        addingAdScreen.addVin(vin);
        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        addingAdScreen.addPrice(price);
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        addingAdScreen.addDescr(descr);
        addingAdScreen.addScrollAndExpand();
        addingAdScreen.addType(type);
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        addingAdScreen.addColour(colour);
        addingAdScreen.addContactPerson(person);
        addingAdScreen.addPhoneNumber(number);
        addingAdScreen.addCheckboxes();
        addingAdScreen.addLocation(voivodeship, city);
        methodHelper.testScreenshot("No possibility to add Ad");
    }

    @Test(dataProvider = "getAttributes", description = "Adding add without required parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad without year of production")
    public void testAddMotoAdNoYear(String email, String password, int first, int second, int third, int fourth,
                                    String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                    boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                    boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                    boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                    boolean electric, String colour, String person, String number, String voivodeship,
                                    String city) {

        System.out.println("Adding motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        addingAdScreen.addAdMotorcycleStart();
        addingAdScreen.addPhoto(first, second, third, fourth);
        addingAdScreen.addBrand(brand);
        addingAdScreen.addModel(model);
        //addingAdScreen.addYearOfProduction(year);
        addingAdScreen.addMileage(mileage);
        addingAdScreen.addVin(vin);
        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        addingAdScreen.addPrice(price);
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        addingAdScreen.addDescr(descr);
        addingAdScreen.addScrollAndExpand();
        addingAdScreen.addType(type);
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        addingAdScreen.addColour(colour);
        addingAdScreen.addContactPerson(person);
        addingAdScreen.addPhoneNumber(number);
        addingAdScreen.addCheckboxes();
        addingAdScreen.addLocation(voivodeship, city);
        methodHelper.testScreenshot("No possibility to add Ad");
    }

    @Test(dataProvider = "getAttributes", description = "Adding add without required parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad without mileage")
    public void testAddMotoAdNoMileage(String email, String password, int first, int second, int third, int fourth,
                                       String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                       boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                       boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                       boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                       boolean electric, String colour, String person, String number, String voivodeship,
                                       String city) {

        System.out.println("Adding motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        addingAdScreen.addAdMotorcycleStart();
        addingAdScreen.addPhoto(first, second, third, fourth);
        addingAdScreen.addBrand(brand);
        addingAdScreen.addModel(model);
        addingAdScreen.addYearOfProduction(year);
//        addingAdScreen.addMileage(mileage);
        addingAdScreen.addVin(vin);
        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        addingAdScreen.addPrice(price);
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        addingAdScreen.addDescr(descr);
        addingAdScreen.addScrollAndExpand();
        addingAdScreen.addType(type);
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        addingAdScreen.addColour(colour);
        addingAdScreen.addContactPerson(person);
        addingAdScreen.addPhoneNumber(number);
        addingAdScreen.addCheckboxes();
        addingAdScreen.addLocation(voivodeship, city);
        methodHelper.testScreenshot("No possibility to add Ad");
    }

    @Test(dataProvider = "getAttributes", description = "Adding add without required parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad without choosing fuel")
    public void testAddMotoAdNoFuel(String email, String password, int first, int second, int third, int fourth,
                                    String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                    boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                    boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                    boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                    boolean electric, String colour, String person, String number, String voivodeship,
                                    String city) {

        System.out.println("Adding motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        addingAdScreen.addAdMotorcycleStart();
        addingAdScreen.addPhoto(first, second, third, fourth);
        addingAdScreen.addBrand(brand);
        addingAdScreen.addModel(model);
        addingAdScreen.addYearOfProduction(year);
        addingAdScreen.addMileage(mileage);
        addingAdScreen.addVin(vin);
//        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        addingAdScreen.addPrice(price);
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        addingAdScreen.addDescr(descr);
        addingAdScreen.addScrollAndExpand();
        addingAdScreen.addType(type);
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        addingAdScreen.addColour(colour);
        addingAdScreen.addContactPerson(person);
        addingAdScreen.addPhoneNumber(number);
        addingAdScreen.addCheckboxes();
        addingAdScreen.addLocation(voivodeship, city);
        methodHelper.testScreenshot("No possibility to add Ad");
    }

    @Test(dataProvider = "getAttributes", description = "Adding add without required parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad without price")
    public void testAddMotoAdNoPrice(String email, String password, int first, int second, int third, int fourth,
                                     String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                     boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                     boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                     boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                     boolean electric, String colour, String person, String number, String voivodeship,
                                     String city) {

        System.out.println("Adding motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        addingAdScreen.addAdMotorcycleStart();
        addingAdScreen.addPhoto(first, second, third, fourth);
        addingAdScreen.addBrand(brand);
        addingAdScreen.addModel(model);
        addingAdScreen.addYearOfProduction(year);
        addingAdScreen.addMileage(mileage);
        addingAdScreen.addVin(vin);
        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        //    addingAdScreen.addPrice(price);
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        addingAdScreen.addDescr(descr);
        addingAdScreen.addScrollAndExpand();
        addingAdScreen.addType(type);
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        addingAdScreen.addColour(colour);
        addingAdScreen.addContactPerson(person);
        addingAdScreen.addPhoneNumber(number);
        addingAdScreen.addCheckboxes();
        addingAdScreen.addLocation(voivodeship, city);
        methodHelper.testScreenshot("No possibility to add Ad");
    }

    @Test(dataProvider = "getAttributes", description = "Adding add without required parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad without description")
    public void testAddMotoAdNoDescr(String email, String password, int first, int second, int third, int fourth,
                                     String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                     boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                     boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                     boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                     boolean electric, String colour, String person, String number, String voivodeship,
                                     String city) {

        System.out.println("Adding motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        addingAdScreen.addAdMotorcycleStart();
        addingAdScreen.addPhoto(first, second, third, fourth);
        addingAdScreen.addBrand(brand);
        addingAdScreen.addModel(model);
        addingAdScreen.addYearOfProduction(year);
        addingAdScreen.addMileage(mileage);
        addingAdScreen.addVin(vin);
        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        addingAdScreen.addPrice(price);
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        //   addingAdScreen.addDescr(descr);
        addingAdScreen.addScrollAndExpand();
        addingAdScreen.addType(type);
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        addingAdScreen.addColour(colour);
        addingAdScreen.addContactPerson(person);
        addingAdScreen.addPhoneNumber(number);
        addingAdScreen.addCheckboxes();
        addingAdScreen.addLocation(voivodeship, city);
        methodHelper.testScreenshot("No possibility to add Ad");
    }

    @Test(dataProvider = "getAttributes", description = "Adding add without required parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad without choosing type")
    public void testAddMotoAdNoType(String email, String password, int first, int second, int third, int fourth,
                                    String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                    boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                    boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                    boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                    boolean electric, String colour, String person, String number, String voivodeship,
                                    String city) {

        System.out.println("Adding motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        addingAdScreen.addAdMotorcycleStart();
        addingAdScreen.addPhoto(first, second, third, fourth);
        addingAdScreen.addBrand(brand);
        addingAdScreen.addModel(model);
        addingAdScreen.addYearOfProduction(year);
        addingAdScreen.addMileage(mileage);
        addingAdScreen.addVin(vin);
        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        addingAdScreen.addPrice(price);
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        addingAdScreen.addDescr(descr);
        addingAdScreen.addScrollAndExpand();
        //  addingAdScreen.addType(type);
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        addingAdScreen.addColour(colour);
        addingAdScreen.addContactPerson(person);
        addingAdScreen.addPhoneNumber(number);
        addingAdScreen.addCheckboxes();
        addingAdScreen.addLocation(voivodeship, city);
        methodHelper.testScreenshot("No possibility to add Ad");
    }

    @Test(dataProvider = "getAttributes", description = "Adding add without required parameters")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks basic functionality - adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad without choosing colour")
    public void testAddMotoAdNoColour(String email, String password, int first, int second, int third, int fourth,
                                      String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                      boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                      boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                      boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                      boolean electric, String colour, String person, String number, String voivodeship,
                                      String city) {

        System.out.println("Adding motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        addingAdScreen.addAdMotorcycleStart();
        addingAdScreen.addPhoto(first, second, third, fourth);
        addingAdScreen.addBrand(brand);
        addingAdScreen.addModel(model);
        addingAdScreen.addYearOfProduction(year);
        addingAdScreen.addMileage(mileage);
        addingAdScreen.addVin(vin);
        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        addingAdScreen.addPrice(price);
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        addingAdScreen.addDescr(descr);
        addingAdScreen.addScrollAndExpand();
        addingAdScreen.addType(type);
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        // addingAdScreen.addColour(colour);
        addingAdScreen.addContactPerson(person);
        addingAdScreen.addPhoneNumber(number);
        addingAdScreen.addCheckboxes();
        addingAdScreen.addLocation(voivodeship, city);
        methodHelper.testScreenshot("No possibility to add Ad");
    }

    @Test(dataProvider = "getAttributes", description = "Adding add with true paramteres")
    @Severity(SeverityLevel.CRITICAL)
    @Description("This test checks adding motorcycle ad")
    @Story("ADDING AD: motorcycle ad without adding location")
    public void testAddMotoAdNoLocation(String email, String password, int first, int second, int third, int fourth,
                                        String brand, String model, String year, String mileage, String vin, boolean gasoline,
                                        boolean diesel, boolean fElectric, String price, boolean nego, boolean vat,
                                        boolean finance, boolean invoice, boolean leasing, String descr, String type,
                                        boolean metalic, boolean pearl, boolean mat, boolean twoStroke, boolean fourStroke,
                                        boolean electric, String colour, String person, String number, String voivodeship,
                                        String city) {

        System.out.println("Adding motorcycle ad test");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        AddingAdScreen addingAdScreen = PageFactory.initElements(driver, AddingAdScreen.class);
        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        methodHelper.waitTime(1);
        registrationScreen.btn_cancelSmartLock.click();
        methodHelper.waitTime(2);
        loginScreen.loginToApp(email, password);
        methodHelper.waitTime(2);
        addingAdScreen.addAdMotorcycleStart();
        addingAdScreen.addPhoto(first, second, third, fourth);
        addingAdScreen.addBrand(brand);
        addingAdScreen.addModel(model);
        addingAdScreen.addYearOfProduction(year);
        addingAdScreen.addMileage(mileage);
        addingAdScreen.addVin(vin);
        addingAdScreen.addFuel(gasoline, diesel, fElectric);
        addingAdScreen.addPrice(price);
        addingAdScreen.addPriceAttributes(nego, vat, finance, invoice, leasing);
        addingAdScreen.addDescr(descr);
        addingAdScreen.addScrollAndExpand();
        addingAdScreen.addType(type);
        addingAdScreen.addEngineType(twoStroke, fourStroke, electric);
        addingAdScreen.addColourParameters(metalic, pearl, mat);
        addingAdScreen.addColour(colour);
        addingAdScreen.addContactPerson(person);
        addingAdScreen.addPhoneNumber(number);
        addingAdScreen.addCheckboxes();
        // addingAdScreen.addLocation(voivodeship, city);
        methodHelper.testScreenshot("No possibility to add Ad");
    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        System.out.println("End of the tests");
    }
}
