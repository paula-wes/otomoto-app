package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.pages.android.SearchConstructionScreen;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;


@Feature("SEARCHING: Construction vehicles and machines search Mobile Tests")
public class SearchConstructionAndroidTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
    }

    //region DATAPROVIDERS

    //region DATAPROVIDER - search machines accessories
    @DataProvider
    public Object[][] searchMachineAccessories() {
        return new Object[][]{
                {"Case", "", "100", "10000", "Prywatne", "Wszystkie", 2, 3, "2005", "2020", "Polska", true, false, true, false, false, false, true, true, true, false, "Małopolskie"}
        };
    }


    //endregion DATAPROVIDER - search machines accessories

    //region DATAPROVIDER - search backhoe loaders
    @DataProvider
    public Object[][] searchBackhoeLoaders() {
        return new Object[][]{
                {"Case", "580ST", "150000", "200000", "Prywatne", "Wszystkie", 2, 3, "2012", "2018", "Polska", 4, 5, "", "", "Kabina", "Koła", 6, 7,
                        "", "", 8, 9, "", "", 10, 11, "", "", false, false, false, true, true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search backhoe loaders

    //region DATAPROVIDER - search bulldozers
    @DataProvider
    public Object[][] searchBulldozers() {
        return new Object[][]{
                {"Case", "", "150000", "200000", "Firma", "PLN", 2, 3, "2012", "2018", "Polska", 4, 5, "50", "100", "Kabina", "Koła", 6, 7,
                        "", "", false, false, false, true, true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search bulldozers

    //region DATAPROVIDER - search cranes and lifts
    @DataProvider
    public Object[][] searchCranesAndLifts() {
        return new Object[][]{
                {"Case", "", "100000", "200000", "Prywatne", "Wszystkie", 2, 3, "2010", "2018", "Polska", false, false, false, true, true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search cranes and lifts

    //region DATAPROVIDER - search tracked excavators
    @DataProvider
    public Object[][] searchTrackedExcavators() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "Firma", "Wszystkie", 2, 3, "2010", "2018", "Polska",
                        false, false, false, true, true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search tracked excavators

    //region DATAPROVIDER - search wheel excavators
    @DataProvider
    public Object[][] searchWheelExcavators() {
        return new Object[][]{
                { "Case", "", "10000", "200000", "", "Wszystkie", 2, 3, "2010", "2018", "Polska", 4, 5, "", "", "", "Koła", 6, 7, "", "", 8, 9, "",
                        "", 10, 11, "", "", false, false, false, true, true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search wheel excavators

    //region DATAPROVIDER - search miniexcavators
    @DataProvider
    public Object[][] searchMiniexcavators() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "", "Wszystkie", 2, 3, "2010", "2018", "Polska", 4, 5, "", "", "", "Koła", 6, 7, "", "", 8, 9, "",
                        "", 10, 11, "", "", false, false, false, true, true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search miniexcavators

    //region DATAPROVIDER - search forklifts
    @DataProvider
    public Object[][] searchForklifts() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "Firma", "Wszystkie", 2, 3, "2010", "2018", "Benzyna", "Polska", 4, 5, "", "", "", 12, "", 13, "",
                        false, false, false, true, true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search forklifts

    //region DATAPROVIDER - search chargers
    @DataProvider
    public Object[][] searchChargers() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "Prywatne", "Wszystkie", 2, 3, "2010", "2018", "Polska", 4, 5, "", "", "","", 6, 7, "", "", 14, 15, "", "",
                        false, false, false, true, true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search chargers

    //region DATAPROVIDER -  search other
    @DataProvider
    public Object[][] searchOther() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "", "Wszystkie", 2, 3, "2010", "2018", "Polska", false, false, false, true,
                        true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search other

    //region DATAPROVIDER - search generators (aggregates)
    @DataProvider
    public Object[][] searchGenerators() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "", "Wszystkie", 2, 3, "2010", "2018", "Polska", 4, 5, "", "", false, false, false, true,
                        true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search generators (aggregates)

    //region DATAPROVIDER - search road machinery
    @DataProvider
    public Object[][] searchRoadMachinery() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "", "", 2, 3, "2010", "2018", "Polska", 4, 5, "", "", 16, 17, "", "", false, false, false, true,
                        true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search road machinery

    //region DATAPROVIDER - search haulers
    @DataProvider
    public Object[][] searchHaulers() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "", "", 2, 3, "2010", "2018", "Polska", "Manualna", 4, 5, "", "", "Kabina", 6, 7, "", "", 14, 15, "", "", false, false, false, true,
                        true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search haulers

    //region DATAPROVIDER - search crushers and screeners
    @DataProvider
    public Object[][] searchCrushersAndScreeners() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "", "Wszystkie", 2, 3, "2010", "2018", "Polska", false, false, false, true,
                        true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search crushers and screeners

    //region DATAPROVIDER - search all in constructions
    @DataProvider
    public Object[][] searchAllInConstructions() {
        return new Object[][]{
                {"Case", "", "10000", "200000", "", "", 2, 3, "2010", "2018", "Polska", false, false, false, true,
                        true, false, false, false, true, true, "Małopolskie"}
        };
    }

    //endregion DATAPROVIDER - search all in constructions

    //endregion DATAPROVIDERS


    //region TESTS
    MethodHelper helper = new MethodHelper();

    //region TEST - machine accessories searching
    @Test(dataProvider = "searchMachineAccessories", description = "Searching item in machine accessories category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search machine accessories")
    public void testMachineAccessoriesSearching(String vehicleBrand, String vehicleModel,
                                                String priceMin, String priceMax, String privacy, String currency,
                                                int third, int fourth, String yearOfProdFrom, String yearOfProdTo, String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                                boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                                boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Akcesoria do maszyn");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestMachineAccessories");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");


    }
    //endregion machine accessories searching

    //region TEST - backhoe loaders searching
    @Test(dataProvider = "searchBackhoeLoaders", description = "Searching item in backhoe loaders category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search backhoe loaders")
    public void testBackhoeLoadersSearching(String vehicleBrand, String vehicleModel,
                                            String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                            String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String powerMin, String powerMax,
                                            String addEquip, String system, int seventh, int eighth, String capacityMin, String capacityMax,
                                            int ninth, int tenth, String bucketCapacityMin, String bucketCapacityMax, int eleventh,
                                            int twelfth, String depthMin, String depthMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                            boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                            boolean noAccidents, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Koparko-ładowarki");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchSetPower(4, 5, powerMin, powerMax);
        constructionScreen.searchSetAdditionalEquipment(addEquip);
        constructionScreen.searchSetRunningSystem(system);
        constructionScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        constructionScreen.searchSetBucketCapacity(8, 9, bucketCapacityMin, bucketCapacityMax);
        constructionScreen.searchSetDiggingDepth(10, 11, depthMin, depthMax);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestBackhoeLoaders");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion backhoe loaders searching

    //region TEST - bulldozers searching
    @Test(dataProvider = "searchBulldozers", description = "Searching item in bulldozers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search bulldozers")
    public void testBulldozersSearching(String vehicleBrand, String vehicleModel,
                                        String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                        String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String powerMin, String powerMax,
                                        String addEquip, String system, int seventh, int eighth, String capacityMin, String capacityMax,
                                        boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                        boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                        boolean noAccidents, String voivodeship) {


        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Spycharki");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchSetPower(4, 5, powerMin, powerMax);
        constructionScreen.searchSetAdditionalEquipment(addEquip);
        constructionScreen.searchSetRunningSystem(system);
        constructionScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestBulldozers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion bulldozers searching

    //region TEST - cranes and lifts searching
    @Test(dataProvider = "searchCranesAndLifts", description = "Searching item in cranes and lifts category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search cranes and lifts")
    public void testCranesAndLiftsSearching(String vehicleBrand, String vehicleModel,
                                            String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                            String yearOfProdFrom, String yearOfProdTo, String country,
                                            boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                            boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                            boolean noAccidents, String voivodeship) {


        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Dźwigi, żurawie, podnośniki");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestCranesAndLifts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion cranes and lifts searching

    //region TEST - tracked excavators searching
    @Test(dataProvider = "searchTrackedExcavators", description = "Searching item in tracked excavators category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search tracked excavators")
    public void testTrackedExcavatorsSearching(String vehicleBrand, String vehicleModel,
                                               String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                               String yearOfProdFrom, String yearOfProdTo, String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                               boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                               boolean noAccidents, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Koparki gąsienicowe");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestTrackedExcavators");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion tracked excavators searching

    //region TEST - wheel excavators searching
    @Test(dataProvider = "searchWheelExcavators", description = "Searching item in wheel excavators category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search wheel excavators")
    public void testWheelExcavatorsSearching(String vehicleBrand, String vehicleModel,
                                             String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                             String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String powerMin, String powerMax,
                                             String addEquip, String system, int seventh, int eighth, String capacityMin, String capacityMax,
                                             int ninth, int tenth, String bucketCapacityMin, String bucketCapacityMax, int eleventh, int twelfth,
                                             String depthMin, String depthMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                             boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                             boolean noAccidents, String voivodeship) {


        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Koparki kołowe");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchSetPower(4, 5, powerMin, powerMax);
        constructionScreen.searchSetAdditionalEquipment(addEquip);
        constructionScreen.searchSetRunningSystem(system);
        constructionScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        constructionScreen.searchSetBucketCapacity(8, 9, bucketCapacityMin, bucketCapacityMax);
        constructionScreen.searchSetDiggingDepth(10, 11, depthMin, depthMax);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestWheelExcavators");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion wheel excavators searching

    //region TEST - miniexcavators searching
    @Test(dataProvider = "searchMiniexcavators", description = "Searching item in miniexcavators category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search miniexcavators")
    public void testMiniexcavatorsSearching(String vehicleBrand, String vehicleModel,
                                            String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                            String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String powerMin, String powerMax,
                                            String addEquip, String system, int seventh, int eighth, String capacityMin, String capacityMax,
                                            int ninth, int tenth, String bucketCapacityMin, String bucketCapacityMax, int eleventh, int twelfth,
                                            String depthMin, String depthMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                            boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                            boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Minikoparki");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchSetPower(4, 5, powerMin, powerMax);
        constructionScreen.searchSetAdditionalEquipment(addEquip);
        constructionScreen.searchSetRunningSystem(system);
        constructionScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        constructionScreen.searchSetBucketCapacity(8, 9, bucketCapacityMin, bucketCapacityMax);
        constructionScreen.searchSetDiggingDepth(10, 11, depthMin, depthMax);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestMiniexcavators");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion miniexcavators searching

    //region TEST - forklifts searching
    @Test(dataProvider = "searchForklifts", description = "Searching item in forklifts category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search forklifts")
    public void testForkliftsSearching(String vehicleBrand, String vehicleModel,
                                       String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                       String yearOfProdFrom, String yearOfProdTo, String fuel, String country, int fifth, int sixth, String powerMin, String powerMax,
                                       String addEquip, int thirteenth, String liftingCapacity, int fourteenth, String liftingHeight,
                                       boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                       boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                       boolean noAccidents, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Wózki widłowe");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetTypeOfFuel(fuel);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchSetPower(4, 5, powerMin, powerMax);
        constructionScreen.searchSetAdditionalEquipment(addEquip);
        constructionScreen.searchSetLiftingCapacity(12, liftingCapacity);
        constructionScreen.searchSetLiftingHeight(13, liftingHeight);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestForklifts");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion forklifts searching

    //region TEST - chargers searching
    @Test(dataProvider = "searchChargers", description = "Searching item in chargers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search chargers")
    public void testChargersSearching(String vehicleBrand, String vehicleModel,
                                      String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                      String yearOfProdFrom, String yearOfProdTo, String country,int fifth, int sixth,  String powerMin, String powerMax,
                                      String addEquip, String system, int seventh, int eighth, String capacityMin, String capacityMax,
                                      int fifteenth, int sixteenth, String capacitanceMin, String capacistanceMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                      boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                      boolean noAccidents, String voivodeship) {


        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Ładowarki");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchSetPower(4, 5, powerMin, powerMax);
        constructionScreen.searchSetAdditionalEquipment(addEquip);
        constructionScreen.searchSetRunningSystem(system);
        constructionScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        constructionScreen.searchSetCapacitance(14, 15, capacitanceMin, capacistanceMax);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestChargers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion chargers searching

    //region TEST - other searching
    @Test(dataProvider = "searchOther", description = "Searching item in other category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search other")
    public void testOtherSearching(String vehicleBrand, String vehicleModel,
                                   String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                   String yearOfProdFrom, String yearOfProdTo, String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                   boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                   boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Inne");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestOther");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion other searching

    //region TEST - generators (aggregates) searching
    @Test(dataProvider = "searchGenerators", description = "Searching item in generators (aggregates) category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search generators (aggregates)")
    public void testGeneratorsSearching(String vehicleBrand, String vehicleModel,
                                        String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                        String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String powerMin, String powerMax,
                                        boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                        boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                        boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Generatory (agregaty)");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchSetPower(4, 5, powerMin, powerMax);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestGenerators");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion generators (aggregates) searching

    //region TEST - road machinery searching
    @Test(dataProvider = "searchRoadMachinery", description = "Searching item in road machinery category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search road machinery")
    public void testRoadMachinerySearching(String vehicleBrand, String vehicleModel,
                                           String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                           String yearOfProdFrom, String yearOfProdTo, String country, int fifth, int sixth, String powerMin, String powerMax,
                                           int seventeenth, int eighteenth, String weightMin, String weightMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                           boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                           boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Maszyny drogowe");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchSetPower(4, 5, powerMin, powerMax);
        constructionScreen.searchSetWeight(16, 17, weightMin, weightMax);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestRoadMachinery");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion road machinery searching

    //region TEST - haulers searching
    @Test(dataProvider = "searchHaulers", description = "Searching item in haulers category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search haulers")
    public void testHaulersSearching(String vehicleBrand, String vehicleModel,
                                     String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                     String yearOfProdFrom, String yearOfProdTo, String country, String gearbox, int fifth,
                                     int sixth, String powerMin, String powerMax, String addEquip, int seventh, int eighth, String capacityMin, String capacityMax,
                                     int fifteenth, int sixteenth, String capacitanceMin, String capacistanceMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                     boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                     boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Wozidła");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchSetGearbox(gearbox);
        constructionScreen.searchSetPower(4, 5, powerMin, powerMax);
        constructionScreen.searchSetAdditionalEquipment(addEquip);
        constructionScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        constructionScreen.searchSetCapacitance(14, 15, capacitanceMin, capacistanceMax);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestHaulers");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion haulers searching

    //region TEST - crushers and screeners searching
    @Test(dataProvider = "searchCrushersAndScreeners", description = "Searching item in crushers and screeners category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search crushers and screeners")
    public void testCrushersAndScreenersSearching(String vehicleBrand, String vehicleModel,
                                                  String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                  String yearOfProdFrom, String yearOfProdTo, String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                                  boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                                  boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Kruszarki i przesiewacze");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestCrushersAndScreeners");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion crushers and screeners searching

    //region TEST - all in constructions searching
    @Test(dataProvider = "searchAllInConstructions", description = "Searching item in all in constructions category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search all in constructions")
    public void testAllInConstructionsSearching(String vehicleBrand, String vehicleModel,
                                                String priceMin, String priceMax, String privacy, String currency, int third, int fourth,
                                                String yearOfProdFrom, String yearOfProdTo, String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                                boolean financingOption, boolean leasing, boolean rent, boolean damaged, boolean registerInPoland, boolean firstOwner,
                                                boolean noAccidents, String voivodeship) {
        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchConstructionScreen constructionScreen = PageFactory.initElements(driver, SearchConstructionScreen.class);

        constructionScreen.searchSetConstructions();
        constructionScreen.searchApplication("Wszystkie w Budowlane");
        constructionScreen.searchVehicleBrand(vehicleBrand);
        constructionScreen.searchVehicleModel(vehicleModel);
        constructionScreen.searchSetPrice(priceMin, priceMax);
        constructionScreen.searchSetPrivacy(privacy);
        constructionScreen.searchSetCurrency(currency);
        constructionScreen.searchMoreParameters();
        constructionScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        constructionScreen.searchSetCountryOfOrigin(country);
        constructionScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        constructionScreen.searchCarStatus(damaged, registerInPoland, firstOwner, noAccidents);
        constructionScreen.searchLocation(voivodeship);
        constructionScreen.searchShowResults();

        helper.testScreenShoot("SearchConstructionAndroidTestAllInConstructions");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion all in constructions searching

    //endregion TESTS


}
