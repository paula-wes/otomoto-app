package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.listeners.TestListener;

import com.selenium.java.pages.web.SearchCarPage;
import io.qameta.allure.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners({TestListener.class})
@Feature("WEB SEARCHING: Car search Web Tests")
public class SearchCarWebTest extends BaseTest {

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = true;
        url = "https://www.otomoto.pl/";

        launchWeb(platform, browser, context);
        System.out.println("Test started");

    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"Opel", "Astra", "F", "1000", "100000", "1999", "2019", "50000", "500000", "Benzyna", true},
                {"BMW", "i8", "", "2000", "200000", "2003", "2019", "30000", "400000", "Benzyna", true},
               {"Audi", "A3", "8P", "1000", "100000", "2005", "2019", "50000", "500000", "Benzyna", true},
                {"Mercedes", "CLS", "C218", "11000", "1000000", "2012", "2019", "50000", "500000", "Diesel", true},
                {"Renault", "Scenic", "II", "1000", "100000", "2000", "2007", "50000", "500000", "Benzyna+LPG", true},
        };
    }

    @Test(dataProvider = "getData", description = "Checking correct searching items")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - correct searching items")
    @Story("SEARCHING: All inputs correct")
    public void searchTest(String brand, String model, String generation, String price_from, String price_to,
                           String year_from, String year_to, String mileage_from, String mileage_to, String fuel,
                           boolean vin) {

        WebDriver driver = getDriver();
        MethodHelper methodHelper = PageFactory.initElements(driver, MethodHelper.class);
        SearchCarPage searchCarPage = PageFactory.initElements(driver, SearchCarPage.class);

        searchCarPage.searchWebBrand(brand);
        System.out.println("Brand set. Now setting model");
        methodHelper.waitTime(2);
        searchCarPage.searchWebModel(model);
        System.out.println("Model set. Now setting generation or price");
        methodHelper.waitTime(2);
        try {
            searchCarPage.searchWebGeneration(generation);
            System.out.println("Generation set. Now setting price");
            methodHelper.waitTime(2);
            searchCarPage.searchWebPrice(price_from, price_to);
            System.out.println("Price set. Now setting year of production");
        } catch (Exception e) {
            searchCarPage.searchWebPrice(price_from, price_to);
            System.out.println("Price set. Now setting year of production");
        }
        methodHelper.waitTime(2);
        searchCarPage.searchWebYear(year_from, year_to);
        System.out.println("Year set. Now setting mileage");
        methodHelper.waitTime(2);
        searchCarPage.searchWebMileage(mileage_from, mileage_to);
        System.out.println("Mileage set. Now setting fuel");
        methodHelper.waitTime(2);
        searchCarPage.searchWebTypeOfFuel(fuel);
        System.out.println("Fuel set. Now setting VIN");
        methodHelper.waitTime(2);
        searchCarPage.searchWebVin(vin);
        System.out.println("VIN set. Now showing results");
        methodHelper.waitTime(2);
        searchCarPage.searchWebShow();
        methodHelper.waitTime(2);
        methodHelper.testScreenshot("Search car");
        searchCarPage.searchWebAssert();
        methodHelper.testScreenshot("searchTest");

    }

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
          driver.close();
    }
}
