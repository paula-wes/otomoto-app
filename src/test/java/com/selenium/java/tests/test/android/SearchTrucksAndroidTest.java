package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.MethodHelper;
import com.selenium.java.pages.android.SearchTrucksScreen;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;

@Feature("SEARCHING: Trucks search Mobile Tests")
public class SearchTrucksAndroidTest extends BaseTest {

    @BeforeClass
    public void startup() {

        startAppiumServer();
    }

    @BeforeMethod(description = "Starting appium server")
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context) {

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
    }

    //region DATAPROVIDERS

    //region DATAPROVIDER - search concrete mixer
    @DataProvider
    public Object[][] searchConcreteMixer() {
        return new Object[][]{
                {
                        "Man", "", "50000", "100000",  "2010", "2015", "Prywatne", "Wszystkie", "", "", "Benzyna", "Polska",  "", "",  "", "",
                        "Manualna", true, "ABS", "Nie", "Biały", "", "",  "", "", false, false,  "", "", "", "", true, false, false, true,
                        false, false, true, false, true, false, false, false, "Małopolskie"
                }

        };
    }

    //endregion DATAPROVIDER - search concrete mixer

    //region DATAPROVIDER - search bus
    @DataProvider
    public Object[][] searchBus() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search bus

    //region DATAPROVIDER - search other
    @DataProvider
    public Object[][] searchOther() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search other

    //region DATAPROVIDER - search tractor unit
    @DataProvider
    public Object[][] searchTractorUnit() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search tractor unit

    //region DATAPROVIDER - search cold store and isotherms
    @DataProvider
    public Object[][] searchColdStoreAndIsotherms() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "Diesel", "Polska", "", "",
                        "", "", "Manualna", false, "ABS", "Nie", "Beżowy", "", "", "", "", "", "", "", "", true, false, true, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search cold store and isotherms

    //region DATAPROVIDER - search cranes, mobile hoists
    @DataProvider
    public Object[][] searchCranesMobileHoists() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "Benzyna", "Polska", "", "",
                        "", "", "Manualna", false, "Elektryczne szyby", "Nie", "Szary", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search cranes, mobile hoists

    //region DATAPROVIDER - search trolley
    @DataProvider
    public Object[][] searchTrolley() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search trolley

    //region DATAPROVIDER - search container
    @DataProvider
    public Object[][] searchContainer() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search container

    //region DATAPROVIDER - search tilt (curtain)
    @DataProvider
    public Object[][] searchTiltCurtain() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "Diesel", "Polska", "", "",
                        "", "", "Automatyczna", false, "ABS", "", "Biały", "", "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search tilt (curtain)

    //region DATAPROVIDER - search concrete pump
    @DataProvider
    public Object[][] searchConcretePump() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "PLN", "", "", "", "Polska", "", "", "",
                        "", "", "Automatyczna", true, "", "Nie", "Biały", "", "", "", "", true, true, "", "", "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Opolskie"}
        };
    }

    //endregion DATAPROVIDER - search concrete pump

    //region DATAPROVIDER - search cistern
    @DataProvider
    public Object[][] searchCistern() {
        return new Object[][]{
                {"ATC", "", "1020", "100000", "2015", "2020", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "", "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search cistern

    //region DATAPROVIDER - search tow
    @DataProvider
    public Object[][] searchTow() {
        return new Object[][]{
                {"ATC", "", "3000", "13333", "2005", "2015", "Prywatne", "PLN", "", "", "Polska", "Diesel", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search tow

    //region DATAPROVIDER - search built-in vehicles
    @DataProvider
    public Object[][] searchBuiltInVehicles() {
        return new Object[][]{
                {"ATC", "", "1000", "12500",  "2004", "2019", "Prywatne", "Wszystkie",  "", "", "", "Polska",  "", "",
                        "", "", "", false, "", "", "", "", "",  "", "", true, false,  "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search built-in vehicles

    //region DATAPROVIDER - search public utility vehicles
    @DataProvider
    public Object[][] searchPublicUtilityVehicles() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie",  "", "", "", "Polska",  "", "",
                      "", "", "", false, "", "", "",  "", "",  "", "", true, false,  "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search public utility vehicles

    //region DATAPROVIDER - search crate
    @DataProvider
    public Object[][] searchCrate() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search crate

    //region DATAPROVIDER - search special
    @DataProvider
    public Object[][] searchSpecial() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search special

    //region DATAPROVIDER - search kit (tractor with semi-trailer / trailer)
    @DataProvider
    public Object[][] searchKit() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search kit (tractor with semi-trailer / trailer)

    //region DATAPROVIDER - search hook lifts and gantries
    @DataProvider
    public Object[][] searchHookLiftsAndGantries() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search hook lifts and gantries

    //region DATAPROVIDER - search truck accessories
    @DataProvider
    public Object[][] searchTruckAccessories() {
        return new Object[][]{
                {"HBM", "", "1000", "10000","2005", "2015", "Prywatne", "Wszystkie", "Polska",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search truck accessories

    //region DATAPROVIDER - search all in trucks
    @DataProvider
    public Object[][] searchAllInTrucks() {
        return new Object[][]{
                {"ATC", "", "1000", "10000", "2005", "2015", "Prywatne", "Wszystkie", "", "", "", "Polska", "", "",
                        "", "", "", false, "", "", "", "", "", "", "", true, false, "", "",
                        false, true, false, true, false, false, false, false, true, true, false, false, "Mazowieckie"}
        };
    }

    //endregion DATAPROVIDER - search all in trucks

    //endregion DATAPROVIDERS


    //region TESTS
    MethodHelper helper = new MethodHelper();

    //region TEST - concrete mixer searching
    @Test(dataProvider = "searchConcreteMixer", description = "Searching item in concrete mixer category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search concrete mixer")
    public void testConcreteMixerSearching(String vehicleBrand,
                                           String vehicleModel,
                                           String priceMin, String priceMax,
                                           String yearOfProdFrom, String yearOfProdTo,
                                           String privacy,
                                           String currency,
                                           String mileageMin, String mileageMax,
                                           String fuel,
                                           String country,
                                           String cubicCapacityMin, String cubicCapacityMax,
                                           String powerMin, String powerMax,
                                           String gearbox,
                                           boolean dieselFilter,
                                           String addEquip,
                                           String steereingWheel,
                                           String colour,
                                           String axlesMin, String axlesMax,
                                           String permissibleWeightMin, String permissibleWeightMax,
                                           boolean nonStandardVec,
                                           boolean rearWheels,
                                           String minBuggyWeight, String maxBuggyWeight,
                                           String capacityMin, String capacityMax,
                                           boolean authorizedDealer,
                                           boolean vatMargin, boolean invoiceVat,
                                           boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin,
                                           boolean damaged, boolean registerInPoland,
                                           boolean noAccidents, boolean servisedASO, String voivodeship) {



        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Betonomieszarka");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestConcreteMixer");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion concrete mixer searching

    //region TEST - bus searching
    @Test(dataProvider = "searchBus", description = "Searching item in bus category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search concrete mixer")
    public void testBusSearching(String vehicleBrand, String vehicleModel,
                                 String priceMin, String priceMax,
                                 String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                 String mileageMin, String mileageMax,
                                 String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                 String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                 String steereingWheel, String colour, String axlesMin, String axlesMax,
                                 String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                 boolean rearWheels,
                                 String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                 boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                 boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Autobus");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestBus");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion bus searching

    //region TEST - other searching
    @Test(dataProvider = "searchOther", description = "Searching item in other category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search other")
    public void testOtherSearching(String vehicleBrand, String vehicleModel,
                                   String priceMin, String priceMax,
                                   String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                   String mileageMin, String mileageMax,
                                   String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                   String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                   String steereingWheel, String colour, String axlesMin, String axlesMax,
                                   String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                   boolean rearWheels, String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                   boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                   boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Inny");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestOther");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion other searching

    //region TEST - tractor unit searching
    @Test(dataProvider = "searchTractorUnit", description = "Searching item in tractor unit category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search tractor unit")
    public void testTractorUnitSearching(String vehicleBrand, String vehicleModel,
                                         String priceMin, String priceMax,
                                         String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                         String mileageMin, String mileageMax,
                                         String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                         String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                         String steereingWheel, String colour, String axlesMin, String axlesMax,
                                         String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                         boolean rearWheels, String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                         boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                         boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Ciągnik siodłowy");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestTractorUnit");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion tractor unit searching

    //region TEST - cold store and isotherms searching
    @Test(dataProvider = "searchColdStoreAndIsotherms", description = "Searching item in cold store and isotherms category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search cold store and isotherms")
    public void testColdStoreAndIsothermsSearching(String vehicleBrand, String vehicleModel,
                                                   String priceMin, String priceMax,
                                                   String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                                   String mileageMin, String mileageMax,
                                                   String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                                   String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                                   String steereingWheel, String colour, String packageMin, String packageCapMax,String capacityCapMin, String capacityCapMax,
                                                   String axlesMin, String axlesMax,
                                                   String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                                   boolean rearWheels, boolean coolAggregate,
                                                   String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                                   boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged,
                                                   boolean registerInPoland, boolean noAccidents, boolean servisedASO, String voivodeship) {


        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Chłodnia i izotermy");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetAllowedPackage(28,29, packageMin,  packageCapMax);
        trucksScreen.searchSetCapacityCap(26,27, capacityCapMin, capacityCapMax);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetCapacityCap(26, 27, capacityCapMin, capacityCapMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetCoolingAggregate(coolAggregate);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestColdstoreAndIsotherms");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion cold store and isotherms searching

    //region TEST - cranes, mobile hoists searching
    @Test(dataProvider = "searchCranesMobileHoists", description = "Searching item in cranes, mobile hoists category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search cranes, mobile hoists")
    public void testCranesMobileHoistsSearching(String vehicleBrand, String vehicleModel,
                                                String priceMin, String priceMax,
                                                String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                                String mileageMin, String mileageMax,
                                                String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                                String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                                String steereingWheel, String colour, String axlesMin, String axlesMax,
                                                String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                                boolean rearWheels,
                                                String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                                boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                                boolean noAccidents, boolean servisedASO, String voivodeship) {



        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Dźwigi, żurawie, podnośniki mobilne");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestCranesMobilesHoists");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion cranes, mobile hoists searching

    //region TEST - trolley searching
    @Test(dataProvider = "searchTrolley", description = "Searching item in trolley category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search trolley")
    public void testTrolleySearching(String vehicleBrand, String vehicleModel,
                                     String priceMin, String priceMax,
                                     String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                     String mileageMin, String mileageMax,
                                     String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                     String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                     String steereingWheel, String colour, String packageMin, String packageCapMax, String axlesMin, String axlesMax,
                                     String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                     boolean rearWheels,
                                     String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                     boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                     boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Wywrotka");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetAllowedPackage(28, 29, packageMin, packageCapMax);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestTrolley");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion trolley searching

    //region TEST - container searching
    @Test(dataProvider = "searchContainer", description = "Searching item in container category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search container")
    public void testContainerSearching(String vehicleBrand, String vehicleModel,
                                       String priceMin, String priceMax,
                                       String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                       String mileageMin, String mileageMax,
                                       String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                       String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                       String steereingWheel, String colour, String axlesMin, String axlesMax,
                                       String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                       boolean rearWheels, String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                       boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                       boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Kontener");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestContainer");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion container searching

    //region TEST - tilt (curtain) searching
    @Test(dataProvider = "searchTiltCurtain", description = "Searching item in tilt (curtain) category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search tilt (curtain)")
    public void testTiltCurtainSearching(String vehicleBrand, String vehicleModel,
                                         String priceMin, String priceMax,
                                         String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                        String mileageMin, String mileageMax,
                                         String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                         String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                         String steereingWheel, String colour,  String packageMin, String packageMax,
                                         String capacityCapMin, String capacityCapMax,
                                         String axlesMin, String axlesMax,
                                         String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                         boolean rearWheels,  String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                         boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                         boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Plandeka (firana)");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetAllowedPackage(28, 29, packageMin, packageMax);
        trucksScreen.searchSetCapacityCap(26, 27, capacityCapMin, capacityCapMax);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestTiltCurtain");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion tilt (curtain) searching

    //region TEST - concrete pump searching
    @Test(dataProvider = "searchConcretePump", description = "Searching item in concrete pump category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search concrete pump")
    public void testConcretePumpSearching(String vehicleBrand, String vehicleModel,
                                          String priceMin, String priceMax,
                                          String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                           String mileageMin, String mileageMax, String fuel,
                                          String country, String range,  String cubicCapacityMin, String cubicCapacityMax,
                                           String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                          String steereingWheel, String colour,  String axlesMin, String axlesMax, String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                          boolean rearWheels,
                                          String minBuggyWeight, String maxBuggyWeight, String capacityMin,
                                          String capacityMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                          boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                          boolean noAccidents, boolean servisedASO, String voivodeship) {


        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Pompa do betonu");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetRange(range);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestConcretePump");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion concrete pump searching

    //region TEST - cistern searching
    @Test(dataProvider = "searchCistern", description = "Searching item in cistern category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search cistern")
    public void testCisternSearching(String vehicleBrand, String vehicleModel,
                                     String priceMin, String priceMax,
                                     String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                     String mileageMin, String mileageMax,
                                     String fuel, String country,  String cubicCapacityMin, String cubicCapacityMax,
                                      String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                     String steereingWheel, String colour,  String axlesMin, String axlesMax,
                                     String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                     boolean rearWheels,
                                     String minBuggyWeight, String maxBuggyWeight,
                                     String capacityMin, String capacityMax, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                     boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                     boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Cysterna");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchSetCapacity(6, 7, capacityMin, capacityMax);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestCistern");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion cistern searching

    //region TEST - tow searching
    @Test(dataProvider = "searchTow", description = "Searching item in tow category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search tow")
    public void testTowSearching(String vehicleBrand, String vehicleModel,
                                 String priceMin, String priceMax,
                                 String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                 String mileageMin, String mileageMax,
                                 String country, String fuel, String cubicCapacityMin, String cubicCapacityMax,
                                 String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                 String steereingWheel, String colour, String axlesMin,  String axlesMax,
                                 String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                 boolean rearWheels,
                                 String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                 boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                 boolean noAccidents, boolean servisedASO, String voivodeship) {


        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Budowlane\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Autolaweta");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestTow");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion tow searching

    //region TEST - built-in vehicles searching
    @Test(dataProvider = "searchBuiltInVehicles", description = "Searching item in built-in vehicles category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search built-in vehicles")
    public void testBuiltInVehiclesSearching(String vehicleBrand, String vehicleModel,
                                             String priceMin, String priceMax,
                                             String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                              String mileageMin, String mileageMax,
                                             String fuel, String country,  String cubicCapacityMin, String cubicCapacityMax,
                                            String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                             String steereingWheel, String colour,  String axlesMin, String axlesMax,
                                             String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                             boolean rearWheels, String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                             boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                             boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Pojazdy do zabudowy");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 18, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(4, 5, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(8, 9, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(22, 23, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestBulitInVehicles");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion built-in vehicles searching

    //region TEST - public utility vehicles searching
    @Test(dataProvider = "searchPublicUtilityVehicles", description = "Searching item in public utility vehicles category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search public utility vehicles")
    public void testPublicUtilityVehiclesSearching(String vehicleBrand, String vehicleModel,
                                                   String priceMin, String priceMax,
                                                   String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                                  String mileageMin, String mileageMax,
                                                   String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                                    String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                                   String steereingWheel, String colour,  String axlesMin, String axlesMax,
                                                   String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                                   boolean rearWheels,String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                                   boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                                   boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Pojazdy użyteczności publicznej");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(6, 7, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestPublicUtilityVehicles");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion public utility vehicles searching

    //region TEST - crate searching
    @Test(dataProvider = "searchCrate", description = "Searching item in crate category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search crate")
    public void testCrateSearching(String vehicleBrand, String vehicleModel,
                                   String priceMin, String priceMax,
                                   String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                   String mileageMin, String mileageMax,
                                   String fuel, String country,String cubicCapacityMin, String cubicCapacityMax,
                                    String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                   String steereingWheel, String colour, String axlesMin, String axlesMax,
                                  String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                   boolean rearWheels,
                                    String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                   boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                   boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Skrzynia");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(6, 7, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestCrate");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion crate searching

    //region TEST - special searching
    @Test(dataProvider = "searchSpecial", description = "Searching item in special category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search special")
    public void testSpecialSearching(String vehicleBrand, String vehicleModel,
                                     String priceMin, String priceMax,
                                     String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                     String mileageMin, String mileageMax,
                                     String fuel, String country,  String cubicCapacityMin, String cubicCapacityMax,
                                     String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                     String steereingWheel, String colour,  String axlesMin, String axlesMax,
                                      String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                     boolean rearWheels,
                                      String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                     boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                     boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Specjalny");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(6, 7, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestSpecial");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion special searching

    //region TEST - kit (tractor with semi-trailer / trailer) searching
    @Test(dataProvider = "searchKit", description = "Searching item in kit (tractor with semi-trailer / trailer) category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search kit (tractor with semi-trailer / trailer)")
    public void testKitSearching(String vehicleBrand, String vehicleModel,
                                 String priceMin, String priceMax,
                                 String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                String mileageMin, String mileageMax,
                                 String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                 String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                 String steereingWheel, String colour,  String axlesMin, String axlesMax,
                                  String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                 boolean rearWheels, String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                 boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                 boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Zestaw (ciągnik z naczepą/przyczepą)");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(6, 7, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestKit");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion kit (tractor with semi-trailer / trailer) searching

    //region TEST - hook lifts and gantries searching
    @Test(dataProvider = "searchHookLiftsAndGantries", description = "Searching item in hook lifts and gantries category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search hook lifts and gantries")
    public void testHookLiftsAndGantriesSearching(String vehicleBrand, String vehicleModel,
                                                  String priceMin, String priceMax,
                                                  String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                                   String mileageMin, String mileageMax,
                                                  String fuel, String country,  String cubicCapacityMin, String cubicCapacityMax,
                                                  String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                                  String steereingWheel, String colour,  String axlesMin, String axlesMax, String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                                  boolean rearWheels, String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                                  boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                                  boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Hakowce i bramowce");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(6, 7, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestHookLiftsAndGantries");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion hook lifts and gantries searching

    //region TEST - truck accessories searching
    @Test(dataProvider = "searchTruckAccessories", description = "Searching item intruck accessories category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search truck accessories")
    public void testTruckAccessoriesSearching(String vehicleBrand, String vehicleModel,
                                              String priceMin, String priceMax,
                                              String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                              String country, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                              boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                              boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Akcesoria do pojazdów ciężarowych");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestTruckAccessories");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");
    }
    //endregion truck accessories searching

    //region TEST - all in trucks searching
    @Test(dataProvider = "searchAllInTrucks", description = "Searching item in all in trucks category")
    @Severity(SeverityLevel.NORMAL)
    @Description("This test checks basic functionality - searching item")
    @Story("SEARCHING: Search all in trucks")
    public void testAllInTrucksSearching(String vehicleBrand, String vehicleModel,
                                         String priceMin, String priceMax,
                                         String yearOfProdFrom, String yearOfProdTo, String privacy, String currency,
                                         String mileageMin, String mileageMax,
                                         String fuel, String country, String cubicCapacityMin, String cubicCapacityMax,
                                         String powerMin, String powerMax, String gearbox, boolean dieselFilter, String addEquip,
                                         String steereingWheel, String colour,  String axlesMin, String axlesMax,
                                         String permissibleWeightMin, String permissibleWeightMax, boolean nonStandardVec,
                                         boolean rearWheels,String minBuggyWeight, String maxBuggyWeight, boolean authorizedDealer, boolean vatMargin, boolean invoiceVat,
                                         boolean financingOption, boolean leasing, boolean rent, boolean registrationNumber, boolean offersWithVin, boolean damaged, boolean registerInPoland,
                                         boolean noAccidents, boolean servisedASO, String voivodeship) {

        System.out.println("Rozpoczęcie testu searchowania w zakładce \"Ciężarowe\"");
        WebDriver driver = getDriver();
        SearchTrucksScreen trucksScreen = PageFactory.initElements(driver, SearchTrucksScreen.class);

        trucksScreen.searchSetTrucks();
        trucksScreen.searchApplication("Wszystkie w Ciężarowe");
        trucksScreen.searchVehicleBrand(vehicleBrand);
        trucksScreen.searchVehicleModel(vehicleModel);
        trucksScreen.searchSetPrice(priceMin, priceMax);
        trucksScreen.searchSetYearOfProduction(2, 3, yearOfProdFrom, yearOfProdTo);
        trucksScreen.searchSetPrivacy(privacy);
        trucksScreen.searchSetCurrency(currency);
        trucksScreen.searchMoreParameters();
        trucksScreen.searchSetMileage(18, 19, mileageMin, mileageMax);
        trucksScreen.searchSetTypeOfFuel(fuel);
        trucksScreen.searchSetCountryOfOrigin(country);
        trucksScreen.searchSetCubicCapacity(20, 21, cubicCapacityMin, cubicCapacityMax);
        trucksScreen.searchSetPower(6, 7, powerMin, powerMax);
        trucksScreen.searchSetGearbox(gearbox);
        trucksScreen.searchSetDieselParticulateFilter(dieselFilter);
        trucksScreen.searchSetAdditionalEquipment(addEquip);
        trucksScreen.searchSetSteeringWheel(steereingWheel);
        trucksScreen.searchSetColour(colour);
        trucksScreen.searchSetNumberOfAxles(8, 9, axlesMin, axlesMax);
        trucksScreen.searchSetPermissibleGrossWeight(22, 23, permissibleWeightMin, permissibleWeightMax);
        trucksScreen.searchSetNonStandardVehicle(nonStandardVec);
        trucksScreen.searchSetDoubleRearWheels(rearWheels);
        trucksScreen.searchSetBuggyWeight(24, 25, minBuggyWeight, maxBuggyWeight);
        trucksScreen.searchFinancialInformation(authorizedDealer, vatMargin, invoiceVat, financingOption, leasing, rent);
        trucksScreen.searchCarStatus(registrationNumber, offersWithVin, damaged, registerInPoland, noAccidents, servisedASO);
        trucksScreen.searchLocation(voivodeship);
        trucksScreen.searchShowResults();

        helper.testScreenShoot("SearchTrucksAndroidTestAllInTrucks");

        assertTrue(driver.findElement(By.id("pl.otomoto:id/activity_search_fake_tab_save_search"))
                .getText().equals("Zapisz filtry wyszukiwania"));
        System.out.println("Test zaliczony - znaleziono wyszukiwane przedmioty według wskazanych parametrów");

    }
    //endregion all in trucks searching


    //endregion TESTS

    @AfterMethod(description = "Finishing test and shutting down app")
    public void tearDown() {
        System.out.println("Koniec testu...");
    }
}
